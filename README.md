ZusiLauncher
============

ZusiLauncher ist ein Fahrplan-Auswahl-Programm für [Zusi3](http://www.zusi.de).

![screenshot main window](images/screenshot1.png)

Der ZusiLauncher kann genutzt werden, um sich einen schnellen Überblick über
die verfügbaren Strecken, Fahrpläne und Strecken verschaffen und auswählen, wo
man fahren will. Zudem beinhaltet es einen "Fahrtenschreiber" der Fahrten
protokolliert und einfache auswertungen erlaubt.

ZusiLauncher bietet zudem eine Suchfunktion, um Fahrten mit bestimmten
Fahrzeugen zu finden.

![screenshot search](images/screenshot-suche.png)

Weitere Suchoptionen sind geplant.

Mit Hilfe einer einfachen Bewertung, kann man Favoriten schnell wieder finden.

![screenshot rating](images/screenshot-rating.png)

Bitte beachten: Das Programm benötigt eine vollwertige Installation von Zusi 3.
Beim ersten Start erscheint ein "Wizard" in dem der Zusi-Daten-Ordner
auszuwählen ist.

![screenshot wizard](images/screenshot4.png)


Funktionsweise des Fahrtenschreibers
------------------------------------

![screenshort fahrtenschreiber](images/screenshot-fahrtenschreiber.png)

Der Fahrtenschreiber verwendet die TCP-Schnittstelle von Zusi3, um Daten zu
protokollieren. Ählnich der Nutzung von externen ZusiDisplays muss disse zur
Nutzung aktiviert werden und evtl. vorhandene Firewall den Zugriff zulassen.
Daten werden nur protokolliert während ZusiLauncher läuft. Wenn ZusiLauncher
eine Verbindung zu Zusi3 hergestellt hat wird die Zusi-Versionsnummer unten
rechts im ZusiLauncher angezeigt. Während Fahrten protokolliert werden, zeigt
ZusiLauncher unten links Daten zur aktuellen Fahrt an.

*Zu beachten:* Die Übersichtslisten zu protokollierten Fahrten werden nicht
direkt aktualisiert. Neu laden sollte die Ergebnisse aber anzeigen. Feature
Request #16 ist dafür aufgemacht.

Der Fahrtenschreiber protokolliert derzeit keine Daten zu Zuglauf o.ä. wenn
der Datenbestand geändert wird, ist die Anzeige folglich fehlerhaft. Das wird
womöglich irgendwann optimiert.


Umgang mit Zusi-Lade-Fehler
---------------------------

Zusi3 hat derzeit einen Fehler, der vehrindert, dass Fahrten direkt über .trn
Dateien gestartet werden, wie es der Zusi-Launcher macht. Ein Klick auf den "Fahren"
Button führt vermutlich zu einem schwarzen Bildschirm. Der Fehler ist im
[Zusi-Forum dokumentiert](https://forum.zusi.de/viewtopic.php?f=55&t=14712).

![screenshot zusi error](images/screenshot-zusi-workaround.png)

Zum umgehen des Fehlers gibt es in Version 0.4.0 einen Button "Fahrplan öffnen".
Wenn dieser Button (1) angeklickt wird, startet Zusi3 in der Fahrplanauswahl mit
der zugehörigen Fahrplandatei. Dabei wird die Zugnummer in die Zwischenablage kopiert,
so dass diese mit Strg+V oder Rechtsklick+Einfügen in das entsprechende Feld (2)
eingetragen werden kann. Dies zeigt dann nur diesen Zug in der Zugliste (3) an und
erlaubt das normale Starten (4).

Zug per TCP starten
-------------------

Wenn der Fahrtenschreiber in der Konfiguration aktiviert ist gibt es einen Button
*Fahren (via TCP)*. Damit der eine Funktion hat muss Zusi gestartet werden, bevor
man auf den Button clickt. ZusiLauncher versucht dann per TCP die Zugfahrt zu
starten. ZusiLauncher meldet dabei keine Kommunikationsfehler. Diese Funktion
ist experimentell. Feedback gerne gesehen.

Eine offene Frage ist, ob Zusi gestartet werden soll, wenn es nicht läuft. Das
wäre machbar, aber win Vorteil der aktuellen Version ist, dass ZusiLauncher auf
einem anderen System laufen kann (so Zugriff auf das Datenverzeichnis existiert)

Einschränkungen
---------------

* Einschränkungen gibt es sicher viele. Eine ist, dass ZusiLauncher derzeit nur ein
Datenverzeichnis kennt. Ticket #13 ist da, um die Erweiterung zu entwickeln.
* Es gibt derzeit keine (vernünftige) Möglichkeit die Konfiguration zu ändern. Wenn
man so z.B. den Fahrtenschreiber beim ersten Start deaktiviert hat oder das Zusi
Datenverzeichnis verschiebt wird, kann das nur durch manuelles editieren der Registry
behoben werden. (Ticket #14)

Hinweis zur Deinstallation
--------------------------

Wenn ZusiLauncher deinstalliert wird werden derzeit nicht alle Daten gelöscht. Im
"AppData" Verzeichnis (je nach System-Konfiguration z.B.
`c:\USers\NAME\AppData\Roaming\Johannes\ZusiLauncher`) befinden sich bis zu drei Dateien.
Je eine für Bewertungen, den Fahrtenschreiber und Cache zum schnelleren Start. Dies können
gelöscht werden, wenn man sie nicht audheben will.

Zudem werden Konfigurationsdaten in der Registry unter
`Computer\HKEY_CURRENT_USER\Software\Johannnes\ZusiLauncher` gespeichert.

Künftige Versionen sollen den (Un-)Installer entsprechend erweitern (Bug #24)

Lizenz
------

Das Programm ist lizensiert unter den Bedingungen der [GNU GPLv3](LICENSE)
Lizenz. Autor ist Johannes Schlüter. Zusi ist eine Marke von Carsten Hölscher.
