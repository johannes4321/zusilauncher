Beiträge zum Code sind sehr willkommen. Wichtig ist, dass die Hauptentwicklung
auf einem Linux-System passiert. Das ist insbesondere bei Pfad-Angaben
relevant, die in Zusi-XML-Dateien mit Backslash angegeben sind und im Progamm
zu einem normalen Slash gewandelt.

Bei Beträgen wird auch gebeten, die Zustimmung zugeben ZusiLauncher unter einer
andere OSI-zertfizierten Open Source Lizenz veröffentlichen zu können.
