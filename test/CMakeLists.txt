cmake_minimum_required(VERSION 3.2)
project ( zusitest )

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC OFF)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5 REQUIRED COMPONENTS Test )

add_executable(zusitest tst_testtest.cpp)
target_link_libraries(zusitest zusilib Qt5::Widgets Qt5::Test )

add_executable(zusitrainratingtest test_trainrating.cpp)
target_link_libraries(zusitrainratingtest zusilib zusilauncherlib Qt5::Test )

set(TEST_ROOT_PATH "${CMAKE_CURRENT_SOURCE_DIR}/data")
if (IS_DIRECTORY ${TEST_ROOT_PATH})
  message(STATUS "Have TEST_ROOT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/data")
else()
  message(FATAL_ERROR "Unable to find TEST_ROOT_PATH")
endif()

#add_executable(zusiuitestsearchwidget searchwidgettest.cpp )
#target_link_libraries(zusiuitestsearchwidget zusilauncherlib Qt5::Test )
#target_include_directories(zusiuitestsearchwidget PRIVATE  ${CMAKE_CURRENT_SOURCE_DIR}/../src/zusilauncher/src ${CMAKE_CURRENT_BINARY_DIR}/../src/zusilauncher)
#target_compile_definitions(zusiuitestsearchwidget PRIVATE -DTEST_ROOT_PATH="${TEST_ROOT_PATH}")

add_test(zusitest zusitest zusitrainratingtest  testzusi3tcp) # zusiuitestsearchwidget)
target_compile_definitions(zusitest PRIVATE -DTEST_ROOT_PATH="${TEST_ROOT_PATH}")
