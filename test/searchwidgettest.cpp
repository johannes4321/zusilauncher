#include <QtTest/QtTest>
#include "searchwidget.h"
#include "ui_searchwidget.h"

#include "fahrplanfinder.h"
#include "fahrplanparser.h"
#include "fahrzeugcache.h"
#include "zugcache.h"

// TODO same old story, get rid of these
ZugCache *globalZugCache;
FahrzeugCache *globalFahrzeugCache;

#ifndef TEST_ROOT_PATH
#error The build system did not provide the TEST_ROOT_PATH define
#endif

static const char *ROOT_PATH = TEST_ROOT_PATH "/";

class SearchWidgetTest : public QObject {
  Q_OBJECT
 public:
  explicit SearchWidgetTest()
      : QObject{},
        zugCache{ROOT_PATH},
        fahrzeugCache{ROOT_PATH},
        model{QString{ROOT_PATH} + "Timetables/", zugCache} {}
 private slots:
  void initTestCase();
  void searchEmptyCriteria();
  void searchZugart();
  void baureihe();
  void variantSearch();

 private:
  ZugCache zugCache;
  FahrzeugCache fahrzeugCache;
  FahrplanItemModel model;
};

void SearchWidgetTest::initTestCase() {
  globalFahrzeugCache = &fahrzeugCache;
  globalZugCache = &zugCache;

  // TODO - We could/should use fake entries here instead of loading all those
  // (copyrighted) XML files
  auto root = model.getRootItem();

  FahrplanParser p{ROOT_PATH, zugCache};
  FahrplanFinder f;
  f.find(QDir(QString{ROOT_PATH} + "Timetables/"));

  foreach (QString fd, f.getList()) { root->addFahrplan(p.read(fd)); }

  model.loadAllZuege();
}

void SearchWidgetTest::searchEmptyCriteria() {
  SearchWidget search{};
  search.setModel(&model);

  QVERIFY(search.ui->listView->model() == nullptr);
  QTest::mouseClick(search.ui->pushButton, Qt::LeftButton);
  QCOMPARE(search.ui->listView->model()->rowCount(), 1508);
}

void SearchWidgetTest::searchZugart() {
  SearchWidget search{};
  search.setModel(&model);

  search.ui->zugtypComboBox->setCurrentIndex(2);
  QCOMPARE(search.ui->zugtypComboBox->currentText(), QString{"Reisezug"});
  QTest::mouseClick(search.ui->pushButton, Qt::LeftButton);
  QCOMPARE(search.ui->listView->model()->rowCount(), 1088);

  search.ui->zugtypComboBox->setCurrentIndex(1);
  QCOMPARE(search.ui->zugtypComboBox->currentText(), QString{"Güterzug"});
  QTest::mouseClick(search.ui->pushButton, Qt::LeftButton);
  QCOMPARE(search.ui->listView->model()->rowCount(), 1508 - 1088);
}

void SearchWidgetTest::baureihe() {
  SearchWidget search{};
  search.setModel(&model);
  search.showForm();

  // In the beginning only "empty" variant
  QCOMPARE(search.ui->varianteComboBox->model()->rowCount(), 1);
  QCOMPARE(search.ui->baureiheComboBox->model()->rowCount(), 128);

  // After selection Baureihe 101 we have an additional variant
  search.ui->baureiheComboBox->setCurrentIndex(1);
  QCOMPARE(search.ui->baureiheComboBox->currentText(), QString("101"));
  QCOMPARE(search.ui->varianteComboBox->model()->rowCount(), 2);
  search.ui->varianteComboBox->setCurrentIndex(1);
  QCOMPARE(search.ui->varianteComboBox->currentText(), QString("101 018"));

  // After resetting it's gone
  search.ui->baureiheComboBox->setCurrentIndex(0);
  QCOMPARE(search.ui->baureiheComboBox->currentText(), QString(""));
  QCOMPARE(search.ui->varianteComboBox->model()->rowCount(), 1);
  QCOMPARE(search.ui->varianteComboBox->currentText(), QString(""));
}

void SearchWidgetTest::variantSearch() {
  SearchWidget search{};
  search.setModel(&model);
  search.showForm();

  // Baureihe 103 has 3 variants + empty
  search.ui->baureiheComboBox->setCurrentIndex(2);
  QCOMPARE(search.ui->baureiheComboBox->currentText(), QString("103"));
  QCOMPARE(search.ui->varianteComboBox->model()->rowCount(), 4);

  QTest::mouseClick(search.ui->pushButton, Qt::LeftButton);
  int totalcount = search.ui->listView->model()->rowCount();
  int sum = 0;
  for (int i = 1; i < 4; ++i) {
    search.ui->varianteComboBox->setCurrentIndex(i);
    QTest::mouseClick(search.ui->pushButton, Qt::LeftButton);
    sum += search.ui->listView->model()->rowCount();
  }
  QCOMPARE(sum, totalcount);
}

QTEST_MAIN(SearchWidgetTest)
#include "searchwidgettest.moc"
