#include <QString>
#include <QtTest>

#include "fahrplanfinder.h"
#include "fahrplanparser.h"
#include "fahrplantree.h"
#include "fahrzeugcache.h"
#include "zug.h"
#include "zugcache.h"

class TestTest : public QObject {
  Q_OBJECT

 public:
  TestTest() = default;

 private Q_SLOTS:
  void emptyZug();
  void load();
};

void TestTest::emptyZug() {
  Zug emptyZug{};

  QTime zeroTime = QTime{0, 0};

  QVERIFY2(emptyZug.getStartzeit() == QDateTime{}, "Wrong Startzeit");
  QVERIFY2(emptyZug.getEndzeit() == QDateTime{}, "Wrong Endzeitzeit");
  QVERIFY2(emptyZug.getFahrzeit() == zeroTime, "Wrong Fahrzeit");
}

ZugCache *globalZugCache;
FahrzeugCache *globalFahrzeugCache;

void TestTest::load() {
  QString rootPath{TEST_ROOT_PATH "/"};
  ZugCache zugCache{rootPath};
  FahrzeugCache fahrzeugCache{rootPath};
  globalZugCache = &zugCache;
  globalFahrzeugCache = &fahrzeugCache;

  FahrplanParser p{rootPath, zugCache};
  FahrplanFinder f;
  f.find(QDir(rootPath + "Timetables/"));

  QVERIFY2(f.getList().length() == 1, "One schedule");
  foreach (QString fd, f.getList()) {
    auto plan = p.read(fd);
    QCOMPARE(rootPath + "Timetables/" + plan->filename, fd);
    QVERIFY2(plan->zugFiles.size() > 0, "At least one train");
  }

  auto plan = p.read(f.getList()[0]);
  auto zuege = plan->getZuege(zugCache);
  QCOMPARE(zuege->size(), plan->zugFiles.size());
  QTRY_COMPARE(zuege->size(), 4);
  // QCOMPARE(zuege[0].getFilename(), rootPath + plan->zugFiles[0]);
}

QTEST_APPLESS_MAIN(TestTest)

#include "tst_testtest.moc"
