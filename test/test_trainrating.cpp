#include <QtTest>

#include "trainrating.h"
#include "zug.h"

class TrainRatingTest : public QObject {
  Q_OBJECT

 private:
  const Zug testZug1{"/file/path1.trn",  false,    "RE", "1234", Zug::Reisezug,
                     "A-Stadt - B-Dorf", "Gruppe", {},   {}};
  const Zug testZug2{"/file/path2.trn",  false,    "RE", "5678", Zug::Gueterzug,
                     "A-Stadt - B-Dorf", "Gruppe", {},   {}};

 private slots:
  void init() {
    auto db = QSqlDatabase::addDatabase("QSQLITE", "ratings");
    db.setDatabaseName(":memory:");
  }
  void cleanup() { QSqlDatabase::removeDatabase("ratings"); }

  void testCanDefaultConstruct() { TrainRating rating{}; }

  void testStoreAndRetrieveRating() {
    TrainRating rating{};
    rating.setRating(testZug1, Rating::POSITIVE);
    QVERIFY(rating.getRating(testZug1) == Rating::POSITIVE);
  }

  void testUnknownReturnsNone() {
    TrainRating rating{};
    QVERIFY(rating.getRating(testZug1) == Rating::NONE);
  }

  void testOverwrite() {
    TrainRating rating{};
    rating.setRating(testZug1, Rating::POSITIVE);
    rating.setRating(testZug1, Rating::NEGATIVE);
    QVERIFY(rating.getRating(testZug1) == Rating::NEGATIVE);
  }

  void testDelete() {
    TrainRating rating{};
    rating.setRating(testZug1, Rating::POSITIVE);
    rating.unsetRating(testZug1);
    QVERIFY(rating.getRating(testZug1) == Rating::NONE);
  }

  void testStoreWithChar() {
    TrainRating rating{};
    rating.setRating(testZug1, "J");
    QVERIFY(rating.getRating(testZug1) == Rating::POSITIVE);
    rating.setRating(testZug1, "K");
    QVERIFY(rating.getRating(testZug1) == Rating::NEUTRAL);
    rating.setRating(testZug1, "L");
    QVERIFY(rating.getRating(testZug1) == Rating::NEGATIVE);
  }

  void testStoreWithInvalidString() {
    TrainRating rating{};
    rating.setRating(testZug1, "Jo");
    QVERIFY(rating.getRating(testZug1) == Rating::NONE);
    rating.setRating(testZug1, "Z");
    QVERIFY(rating.getRating(testZug1) == Rating::NONE);
  }

  void testReadInvalidValueFromDB() {
    TrainRating rating{};

    QSqlQuery query{"REPLACE INTO ratings (fahrplan, rating) VALUES (?, ?)",
                    QSqlDatabase::database("ratings", true)};
    query.bindValue(0, testZug1.getFilename());
    query.bindValue(1, "M");
    query.exec();
    QVERIFY(rating.getRating(testZug1) == Rating::NONE);

    query.bindValue(1, "JJ");
    query.exec();
    QVERIFY(rating.getRating(testZug1) == Rating::NONE);
  }

  void testDifferentTrainsDontOverwrite() {
    TrainRating rating{};
    rating.setRating(testZug1, Rating::POSITIVE);
    rating.setRating(testZug2, Rating::NEGATIVE);

    QVERIFY(rating.getRating(testZug1) == Rating::POSITIVE);
    QVERIFY(rating.getRating(testZug2) == Rating::NEGATIVE);
  }
};

#include "test_trainrating.moc"

QTEST_APPLESS_MAIN(TrainRatingTest)
