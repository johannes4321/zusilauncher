<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>FahrplanItemModel</name>
    <message>
        <location filename="../src/fahrplanitemmodel.cpp" line="107"/>
        <source>Zuglauf</source>
        <translation>Zuglauf</translation>
    </message>
    <message>
        <location filename="../src/fahrplanitemmodel.cpp" line="120"/>
        <source>Lade Züge ... </source>
        <translation>Lade Züge ... </translation>
    </message>
    <message>
        <location filename="../src/fahrplanitemmodel.cpp" line="120"/>
        <source>Abbrechen (zwecklos)</source>
        <translation>Abbrechen (zwecklos)</translation>
    </message>
    <message>
        <location filename="../src/fahrplanitemmodel.cpp" line="122"/>
        <source>Lade Züge</source>
        <translation>Lade Züge</translation>
    </message>
    <message>
        <location filename="../src/fahrplanitemmodel.cpp" line="160"/>
        <source>Anzahl der Züge: %1
Davon Deko: %2

Züge ohne Fahrzeuge (ohne Deko): %3
%4
Züge mit Führungsfahrzeug ohne EinsatzAb (ohne Deko): %5
%6</source>
        <translation>Anzahl der Züge: %1
Davon Deko: %2

Züge ohne Fahrzeuge (ohne Deko): %3
%4
Züge mit Führungsfahrzeug ohne EinsatzAb (ohne Deko): %5
%6</translation>
    </message>
</context>
<context>
    <name>Fahrtenschreiber::Fahrtenschreiber</name>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="123"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="124"/>
        <source>Startzeit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="125"/>
        <source>Messpunkte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="126"/>
        <source>Gefahrene Strecke</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="153"/>
        <source>Geschwindigkeit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="155"/>
        <source>Streckengeschwindigkeit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fahrtenschreiber::FahrtenschreiberWidget</name>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiberwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fahrtenschreiber::FullReportModel</name>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="25"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="26"/>
        <source>Startzeit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="27"/>
        <source>Zug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="28"/>
        <source>Messpunkte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="29"/>
        <source>Gefahrene Strecke</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fahrtenschreiber::ReportForm</name>
    <message>
        <location filename="../src/fahrtenschreiber/reportform.ui" line="14"/>
        <source>Fahrtenschreiber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/reportform.cpp" line="30"/>
        <source>ZusiLauncher Fahrtenschreiber - %1 %2 %3 (%4)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FirstLaunchWizard</name>
    <message>
        <location filename="../src/firstlaunch/firstlaunchwizard.cpp" line="19"/>
        <source>ZusiLauncher Setup</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LaunchErrorForm</name>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="14"/>
        <location filename="../src/launcherror/launcherrorform.ui" line="29"/>
        <source>Fehler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="42"/>
        <source>Zusi konnte nicht gestartet werden!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="49"/>
        <source>Folgende Datei sollte geladen werden:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="56"/>
        <source>(Beschreibung)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="70"/>
        <source>Datei im Explorer anzeigen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="77"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Vermutlich sind Zusi-Dateien nicht mit dem Zusi Fahrsimulator &lt;span style=&quot; font-style:italic;&quot;&gt;ZusiSim.exe&lt;/span&gt; verknüft. Diese Verknüpfung muss im Windows-System hergestellt werden.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;Eine Verknüpfung kann hergestellt werden, in dem man im Explorer veruscht die Datei mit Doppelklick aufzurufen. Das Windows-System sollte dann nach der zugehörigen Anwendung fragen. Dort ist der Zusi Fahrsimulator ZusiSim.exe auszusuchen.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;Sollte die Verknüpfung existieren und das Starten denoch nicht funktionieren bitte im Zusi-Forum, unter Angabe der Windows-, Zusi- und ZusiLauncher-Version und evtl. sonstiger Konfiguration, um Hilfe bitten.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="87"/>
        <source>Nochmal probieren</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.cpp" line="48"/>
        <source>Explorer konnte nicht gestartet werden.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.cpp" line="59"/>
        <source>Das ging wohl wieder schief ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LauncherWindow</name>
    <message>
        <location filename="../src/launcherwindow.ui" line="14"/>
        <location filename="../src/launcherwindow.ui" line="90"/>
        <source>ZusiLauncher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="35"/>
        <source>&amp;Fahrpläne</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="58"/>
        <source>Zug&amp;suche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="63"/>
        <source>Fahrtenschreiber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="97"/>
        <source>Bitte einen Zug auswählen!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="133"/>
        <source>&amp;Datei</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="141"/>
        <source>&amp;Über</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="148"/>
        <source>Entwickleroptionen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="159"/>
        <source>Be&amp;enden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="162"/>
        <source>Ctrl+Q</source>
        <translation>Strg+Q</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="167"/>
        <source>Über &amp;Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="172"/>
        <source>Über &amp;ZusiLauncher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="177"/>
        <source>Datenbestand analysieren</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="182"/>
        <source>Protokoll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.cpp" line="24"/>
        <source>Keine Daten geladen (model is NULL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.cpp" line="70"/>
        <source>Lade ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.cpp" line="103"/>
        <source>Zusi beendet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.cpp" line="151"/>
        <source>Init time: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.cpp" line="166"/>
        <source>Fahrplan geladen: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProtocolForm</name>
    <message>
        <location filename="../src/fahrtenschreiber/protocolform.ui" line="14"/>
        <source>Protocol</source>
        <translation>Protokoll</translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/protocolform.ui" line="25"/>
        <source>Clear</source>
        <translation>Leeren</translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/protocolform.ui" line="32"/>
        <source>Store</source>
        <translation>Speichern</translation>
    </message>
</context>
<context>
    <name>SearchBetriebsstelle</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="26"/>
        <source>Durchfahrene Betriebsstelle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchDuration</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="189"/>
        <source>Fahrtdauer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="196"/>
        <source>Minuten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="198"/>
        <source>mindestens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="199"/>
        <source>maximal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchEngineAge</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="246"/>
        <source>Ersteinsatz der Lok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="254"/>
        <source>nach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="255"/>
        <source>vor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchEngineType</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="106"/>
        <source>Baureihe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchRating</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="293"/>
        <source>Bewertung</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <location filename="../src/searchwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/searchwidget.ui" line="24"/>
        <source>Suchoptionen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/searchwidget.ui" line="42"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/searchwidget.ui" line="49"/>
        <source>Zurücksetzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/searchwidget.ui" line="85"/>
        <source>fsdfsd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/searchwidget.ui" line="30"/>
        <source>Dekozüge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zugnummer</source>
        <translation type="vanished">Zugnummer</translation>
    </message>
    <message>
        <location filename="../src/searchwidget.ui" line="61"/>
        <source>Suchen</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchZDA</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="318"/>
        <source>ZDA Daten (Ansagen, experimentell)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="321"/>
        <source>ZDA Daten gewünscht</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchZugType</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="39"/>
        <source>Zugart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="44"/>
        <source>Reisezug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="45"/>
        <source>Güterzug</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchZugnummer</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="58"/>
        <source>Zugnummer</source>
        <translation type="unfinished">Zugnummer</translation>
    </message>
</context>
<context>
    <name>TcpPage</name>
    <message>
        <location filename="../src/firstlaunch/tcppage.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/tcppage.ui" line="20"/>
        <source>Fahrtenschreiber aktivieren</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/tcppage.ui" line="32"/>
        <source>Der Fahrtenschreiber protokolliert alle Fahrten. Die vorgegebenen Werte müssen nur geändern werden, wenn Zusi besonders konfiguriert wurde.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/tcppage.ui" line="44"/>
        <source>Hostname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/tcppage.ui" line="51"/>
        <source>127.0.0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/tcppage.ui" line="58"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrainWidget</name>
    <message>
        <location filename="../src/trainwidget.ui" line="539"/>
        <source>Fahrplan Öffnen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/trainwidget.ui" line="546"/>
        <source>Fahren (via TCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/trainwidget.ui" line="553"/>
        <source>Fahren!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/trainwidget.cpp" line="178"/>
        <source>ZDA Daten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/trainwidget.cpp" line="222"/>
        <source>unbekannter Fahrplan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/trainwidget.cpp" line="225"/>
        <source>unbekannte Gruppe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/trainwidget.cpp" line="226"/>
        <source>unbekannter Plan</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WelcomePage</name>
    <message>
        <location filename="../src/firstlaunch/welcomepage.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/welcomepage.ui" line="17"/>
        <source>ZusiLauncher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/welcomepage.ui" line="20"/>
        <source>Willkommen beim ZusiLauncher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/welcomepage.ui" line="26"/>
        <source>Mit diesem Wizard wird ZusiLauncher, das Fahrplanauswahlprogamm für Zusi3, einmalig konfiguriert. Keine Angst, es geht schnell!

Dieses Programm wurde von Johannes Schlüter entwickelt und wird unter den Bedingungen der GNU General Public License 3 verbreitet. Zusi ist eine registrierte Marke von Zusi Bahnsimulatoren Carsten Hölscher Software.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ZusiFinderPage</name>
    <message>
        <location filename="../src/firstlaunch/zusifinderpage.cpp" line="31"/>
        <source>Open Directory</source>
        <translation>Verzeichnis öffnen</translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/zusifinderpage.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/zusifinderpage.ui" line="17"/>
        <source>ZusiLauncher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/zusifinderpage.ui" line="20"/>
        <source>Wo sind die Zusi-Daten?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/zusifinderpage.ui" line="26"/>
        <source>Wo finden wir die Zusi-Daten? - Bei einer normalen Installation von Zusi sollte der vorgeschlagene Pfad stimmen.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/zusifinderpage.ui" line="41"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
