<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>FahrplanItemModel</name>
    <message>
        <location filename="../src/fahrplanitemmodel.cpp" line="107"/>
        <source>Zuglauf</source>
        <translation>Train Route</translation>
    </message>
    <message>
        <location filename="../src/fahrplanitemmodel.cpp" line="120"/>
        <source>Lade Züge ... </source>
        <translation>Loading Trains ... </translation>
    </message>
    <message>
        <location filename="../src/fahrplanitemmodel.cpp" line="120"/>
        <source>Abbrechen (zwecklos)</source>
        <translation>Cacnel (won&apos;t work)</translation>
    </message>
    <message>
        <location filename="../src/fahrplanitemmodel.cpp" line="122"/>
        <source>Lade Züge</source>
        <translation>Loading Trains</translation>
    </message>
    <message>
        <location filename="../src/fahrplanitemmodel.cpp" line="160"/>
        <source>Anzahl der Züge: %1
Davon Deko: %2

Züge ohne Fahrzeuge (ohne Deko): %3
%4
Züge mit Führungsfahrzeug ohne EinsatzAb (ohne Deko): %5
%6</source>
        <translation>Number of trains: %1
decoration: %2

Trains without leading vehicle(without decoration): %3
%4
Trains with a leading vehicle without EinsatzAb (without deco): %5
%6</translation>
    </message>
</context>
<context>
    <name>Fahrtenschreiber::Fahrtenschreiber</name>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="123"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="124"/>
        <source>Startzeit</source>
        <translation>Starting Time</translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="125"/>
        <source>Messpunkte</source>
        <translation>Measurements</translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="126"/>
        <source>Gefahrene Strecke</source>
        <translation>Distance Driven</translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="153"/>
        <source>Geschwindigkeit</source>
        <translation>Speed</translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="155"/>
        <source>Streckengeschwindigkeit</source>
        <translation>Max. route speed</translation>
    </message>
</context>
<context>
    <name>Fahrtenschreiber::FahrtenschreiberWidget</name>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiberwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fahrtenschreiber::FullReportModel</name>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="25"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="26"/>
        <source>Startzeit</source>
        <translation>Starting Time</translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="27"/>
        <source>Zug</source>
        <translation>Train</translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="28"/>
        <source>Messpunkte</source>
        <translation>Measurements</translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/fahrtenschreiber.cpp" line="29"/>
        <source>Gefahrene Strecke</source>
        <translation>Distance Driven</translation>
    </message>
</context>
<context>
    <name>Fahrtenschreiber::ReportForm</name>
    <message>
        <location filename="../src/fahrtenschreiber/reportform.ui" line="14"/>
        <source>Fahrtenschreiber</source>
        <translation>Data Recorder</translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/reportform.cpp" line="30"/>
        <source>ZusiLauncher Fahrtenschreiber - %1 %2 %3 (%4)</source>
        <translation>ZusiLauncher Data Recorder - %1 %2 %3 (%4)</translation>
    </message>
</context>
<context>
    <name>FirstLaunchWizard</name>
    <message>
        <location filename="../src/firstlaunch/firstlaunchwizard.cpp" line="19"/>
        <source>ZusiLauncher Setup</source>
        <translation>ZusiLauncher Setup</translation>
    </message>
</context>
<context>
    <name>LaunchErrorForm</name>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="14"/>
        <location filename="../src/launcherror/launcherrorform.ui" line="29"/>
        <source>Fehler</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="42"/>
        <source>Zusi konnte nicht gestartet werden!</source>
        <translation>Zusi could not be launched!</translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="49"/>
        <source>Folgende Datei sollte geladen werden:</source>
        <translation>This file should have been loaded:</translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="56"/>
        <source>(Beschreibung)</source>
        <translation>(Description)</translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="70"/>
        <source>Datei im Explorer anzeigen</source>
        <translation>Show in Explorer</translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="77"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Vermutlich sind Zusi-Dateien nicht mit dem Zusi Fahrsimulator &lt;span style=&quot; font-style:italic;&quot;&gt;ZusiSim.exe&lt;/span&gt; verknüft. Diese Verknüpfung muss im Windows-System hergestellt werden.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;Eine Verknüpfung kann hergestellt werden, in dem man im Explorer veruscht die Datei mit Doppelklick aufzurufen. Das Windows-System sollte dann nach der zugehörigen Anwendung fragen. Dort ist der Zusi Fahrsimulator ZusiSim.exe auszusuchen.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;Sollte die Verknüpfung existieren und das Starten denoch nicht funktionieren bitte im Zusi-Forum, unter Angabe der Windows-, Zusi- und ZusiLauncher-Version und evtl. sonstiger Konfiguration, um Hilfe bitten.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Probably Zusi files ren&apos;T associated to the Zusi Simulator &lt;span style=&quot; font-style:italic;&quot;&gt;ZusiSim.exe&lt;/span&gt;.This association has to be registered to the Windows system.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;Such a association can be created by double clicking on a Zusi file in Windows Explorer, then Windows will ask for an application to use.There you can chose the ZusiSim.exe simulator file.&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;Should this association exist, but ZusiLauncher still can&apos;t launch Zusi please report to the Zusi forum, mentioning Windows-, Zusi- und ZusiLauncher-versions uand other relevant system information.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.ui" line="87"/>
        <source>Nochmal probieren</source>
        <translation>Try again</translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.cpp" line="48"/>
        <source>Explorer konnte nicht gestartet werden.</source>
        <translation>Explorer could not be launched.</translation>
    </message>
    <message>
        <location filename="../src/launcherror/launcherrorform.cpp" line="59"/>
        <source>Das ging wohl wieder schief ...</source>
        <translation>Apparently that still didn&apos;t work ...</translation>
    </message>
</context>
<context>
    <name>LauncherWindow</name>
    <message>
        <location filename="../src/launcherwindow.ui" line="14"/>
        <location filename="../src/launcherwindow.ui" line="90"/>
        <source>ZusiLauncher</source>
        <translation>ZusiLauncher</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="35"/>
        <source>&amp;Fahrpläne</source>
        <translation>&amp;Ttimtetables</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="58"/>
        <source>Zug&amp;suche</source>
        <translation>&amp;Search Train</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="63"/>
        <source>Fahrtenschreiber</source>
        <translation>Data Recorder</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="97"/>
        <source>Bitte einen Zug auswählen!</source>
        <translation>Please choose a train!</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="133"/>
        <source>&amp;Datei</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="141"/>
        <source>&amp;Über</source>
        <translation>&amp;About</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="148"/>
        <source>Entwickleroptionen</source>
        <translation>Developer options</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="159"/>
        <source>Be&amp;enden</source>
        <translation>&amp;Quit</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="162"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="167"/>
        <source>Über &amp;Qt</source>
        <translation>About &amp;Qt</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="172"/>
        <source>Über &amp;ZusiLauncher</source>
        <translation>About &amp;ZusiLauncher</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="177"/>
        <source>Datenbestand analysieren</source>
        <translation>Analyse Data</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.ui" line="182"/>
        <source>Protokoll</source>
        <translation>Protocol</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.cpp" line="24"/>
        <source>Keine Daten geladen (model is NULL)</source>
        <translation>No data loaded (model is NULL)</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.cpp" line="70"/>
        <source>Lade ...</source>
        <translation>Loading ...</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.cpp" line="103"/>
        <source>Zusi beendet</source>
        <translation>Zusi terminated</translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.cpp" line="151"/>
        <source>Init time: </source>
        <translation>Init time: </translation>
    </message>
    <message>
        <location filename="../src/launcherwindow.cpp" line="166"/>
        <source>Fahrplan geladen: </source>
        <translation>Timetable loaded: </translation>
    </message>
</context>
<context>
    <name>ProtocolForm</name>
    <message>
        <location filename="../src/fahrtenschreiber/protocolform.ui" line="14"/>
        <source>Protocol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/protocolform.ui" line="25"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/fahrtenschreiber/protocolform.ui" line="32"/>
        <source>Store</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchBetriebsstelle</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="26"/>
        <source>Durchfahrene Betriebsstelle</source>
        <translation>Location (Betriebsstelle)</translation>
    </message>
</context>
<context>
    <name>SearchDuration</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="189"/>
        <source>Fahrtdauer</source>
        <translation>Duration</translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="196"/>
        <source>Minuten</source>
        <translation>minutes</translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="198"/>
        <source>mindestens</source>
        <translation>at least</translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="199"/>
        <source>maximal</source>
        <translation>at most</translation>
    </message>
</context>
<context>
    <name>SearchEngineAge</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="246"/>
        <source>Ersteinsatz der Lok</source>
        <translation>Age of Engine</translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="254"/>
        <source>nach</source>
        <translation>newer than</translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="255"/>
        <source>vor</source>
        <translation>older than</translation>
    </message>
</context>
<context>
    <name>SearchEngineType</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="106"/>
        <source>Baureihe</source>
        <translation>Engine Type</translation>
    </message>
</context>
<context>
    <name>SearchRating</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="293"/>
        <source>Bewertung</source>
        <translation>Rating</translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <location filename="../src/searchwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/searchwidget.ui" line="24"/>
        <source>Suchoptionen</source>
        <translation>Search Options</translation>
    </message>
    <message>
        <location filename="../src/searchwidget.ui" line="30"/>
        <source>Dekozüge</source>
        <translation>Decorative Trains</translation>
    </message>
    <message>
        <location filename="../src/searchwidget.ui" line="42"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/searchwidget.ui" line="49"/>
        <source>Zurücksetzen</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="../src/searchwidget.ui" line="61"/>
        <source>Suchen</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="../src/searchwidget.ui" line="85"/>
        <source>fsdfsd</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchZDA</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="318"/>
        <source>ZDA Daten (Ansagen, experimentell)</source>
        <translation>ZDA DAta (Announcments, experimental)</translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="321"/>
        <source>ZDA Daten gewünscht</source>
        <translation>ZDA Data Required</translation>
    </message>
</context>
<context>
    <name>SearchZugType</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="39"/>
        <source>Zugart</source>
        <translation>Train Type</translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="44"/>
        <source>Reisezug</source>
        <translation>Passanger Train</translation>
    </message>
    <message>
        <location filename="../src/search/searchoption.cpp" line="45"/>
        <source>Güterzug</source>
        <translation>Freight Train</translation>
    </message>
</context>
<context>
    <name>SearchZugnummer</name>
    <message>
        <location filename="../src/search/searchoption.cpp" line="58"/>
        <source>Zugnummer</source>
        <translation>Train Number</translation>
    </message>
</context>
<context>
    <name>TcpPage</name>
    <message>
        <location filename="../src/firstlaunch/tcppage.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/tcppage.ui" line="20"/>
        <source>Fahrtenschreiber aktivieren</source>
        <translation type="unfinished">Enable Data Recorder</translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/tcppage.ui" line="32"/>
        <source>Der Fahrtenschreiber protokolliert alle Fahrten. Die vorgegebenen Werte müssen nur geändern werden, wenn Zusi besonders konfiguriert wurde.</source>
        <translation type="unfinished">The data recorder records train rides. The default values should only be changed, if Zusi was configured in non-default ways.</translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/tcppage.ui" line="44"/>
        <source>Hostname</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/tcppage.ui" line="51"/>
        <source>127.0.0.1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/tcppage.ui" line="58"/>
        <source>Port</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TrainWidget</name>
    <message>
        <location filename="../src/trainwidget.ui" line="539"/>
        <source>Fahrplan Öffnen</source>
        <translation>Open Train Table</translation>
    </message>
    <message>
        <location filename="../src/trainwidget.ui" line="546"/>
        <source>Fahren (via TCP)</source>
        <translation>Drive (via TCP)</translation>
    </message>
    <message>
        <location filename="../src/trainwidget.ui" line="553"/>
        <source>Fahren!</source>
        <translation>Drive Train!</translation>
    </message>
    <message>
        <location filename="../src/trainwidget.cpp" line="178"/>
        <source>ZDA Daten</source>
        <translation>ZDA Data</translation>
    </message>
    <message>
        <location filename="../src/trainwidget.cpp" line="222"/>
        <source>unbekannter Fahrplan</source>
        <translation>Unknown Timetable</translation>
    </message>
    <message>
        <location filename="../src/trainwidget.cpp" line="225"/>
        <source>unbekannte Gruppe</source>
        <translation>Unknown Group</translation>
    </message>
    <message>
        <location filename="../src/trainwidget.cpp" line="226"/>
        <source>unbekannter Plan</source>
        <translation>unknown Plan</translation>
    </message>
</context>
<context>
    <name>WelcomePage</name>
    <message>
        <location filename="../src/firstlaunch/welcomepage.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/welcomepage.ui" line="17"/>
        <source>ZusiLauncher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/welcomepage.ui" line="20"/>
        <source>Willkommen beim ZusiLauncher</source>
        <translation>Welcome to ZusiLauncher</translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/welcomepage.ui" line="26"/>
        <source>Mit diesem Wizard wird ZusiLauncher, das Fahrplanauswahlprogamm für Zusi3, einmalig konfiguriert. Keine Angst, es geht schnell!

Dieses Programm wurde von Johannes Schlüter entwickelt und wird unter den Bedingungen der GNU General Public License 3 verbreitet. Zusi ist eine registrierte Marke von Zusi Bahnsimulatoren Carsten Hölscher Software.</source>
        <translation>This Wizard will configure ZusiLauncher, the time table picking program for Zusi3. No worrires, we will be quick!

This program was developed by Johannes Schlüter and has been licensed under the terms of the GNU General Public License 3. Zusi is a registererted trademark of Zusi Bahnsimulatoren Carsten Hölscher Software.</translation>
    </message>
</context>
<context>
    <name>ZusiFinderPage</name>
    <message>
        <location filename="../src/firstlaunch/zusifinderpage.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/zusifinderpage.ui" line="17"/>
        <source>ZusiLauncher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/zusifinderpage.ui" line="20"/>
        <source>Wo sind die Zusi-Daten?</source>
        <translation>Where is your Zusi Data folder?</translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/zusifinderpage.ui" line="26"/>
        <source>Wo finden wir die Zusi-Daten? - Bei einer normalen Installation von Zusi sollte der vorgeschlagene Pfad stimmen.</source>
        <translation>Where can we find the Zusi data directory? For normal installations the suggested path should work.</translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/zusifinderpage.ui" line="41"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/firstlaunch/zusifinderpage.cpp" line="31"/>
        <source>Open Directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
