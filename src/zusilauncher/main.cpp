#include "src/launcherwindow.h"

#include <iostream>
#include "fahrplanfinder.h"
#include "fahrplanparser.h"
#include "fahrzeugcache.h"
#include "microbench.h"
#include "src/fahrplanitemmodel.h"
#include "trainrating.h"
#include "zugcache.h"

#include "zusiwatcher.h"

#include <QApplication>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QStateMachine>
#include <QThread>
#include <QTranslator>
#include <QWizard>
#include <QtSql>

#include "src/fahrtenschreiber/fahrtenschreiber.h"
#include "src/firstlaunch/firstlaunchwizard.h"
#include "src/languagepicker/languagepickerdialog.h"

#include "zusicache.h"

ZugCache *globalZugCache;
FahrzeugCache *globalFahrzeugCache;

class LanguageEnsurer final : public QObject {
  std::unique_ptr<LanguagePickerDialog> picker{nullptr};
  Q_OBJECT

 signals:
  void languageSet();
  void languageChoiceAborted();

 public slots:
  void findLanguage() {
    QSettings settings{};
    auto languageSetting = settings.value("config/language");

    picker = std::make_unique<LanguagePickerDialog>();
    if (!languageSetting.isNull()) {
      languageChanged(languageSetting.toString());
    } else {
      connect(picker.get(), &LanguagePickerDialog::accepted, this,
              &LanguageEnsurer ::languageChanged);
      connect(picker.get(), &LanguagePickerDialog::rejected, this,
              &LanguageEnsurer ::languageChoiceAborted);
      picker->open();
    }
  }

 private slots:
  void languageChanged(const QString &language) {
    auto translator = new QTranslator(this);
    translator->load(qApp->applicationDirPath() +
                     "/translations/zusilauncher_" + language);
    qApp->installTranslator(translator);

    // TODO: This doesn't really belong here and should only be run if a new
    // choice was made ...
    QSettings settings{};
    settings.setValue("config/language", language);
    settings.sync();

    emit languageSet();
  }
};

class ConfigEnsurer final : public QObject {
  std::unique_ptr<FirstLaunchWizard> wizard{nullptr};
  Q_OBJECT

 signals:
  void haveConfig();
  void configAborted();

 public slots:

  void ensureConfig() {
    QSettings settings;

    int configVersion = settings.value("config/version").toInt();

    if (configVersion != 2) {
      launchWizard();
      return;
    }

    emit haveConfig();
  }

 private slots:
  void launchWizard() {
    wizard = std::make_unique<FirstLaunchWizard>();
    connect(wizard.get(), &QDialog::accepted, this,
            &ConfigEnsurer::wizardAccepted);
    connect(wizard.get(), &QDialog::rejected, this,
            &ConfigEnsurer::configAborted);
    wizard->open();
  }

  void wizardAccepted() {
    QSettings settings;
    settings.setValue("config/version", 2);
    settings.sync();

    emit haveConfig();
  }
};

class ZusiLauncher : public QObject {
  Q_OBJECT
 public:
  ZusiLauncher() {
    QCoreApplication::setOrganizationName("Johannnes");
    QCoreApplication::setOrganizationDomain("schlueters.de");
    QCoreApplication::setApplicationName("ZusiLauncher");

    // do we have to free those? can we put them on stack?
    auto languageEnsurer = new LanguageEnsurer{};
    auto configEnsurer = new ConfigEnsurer{};
    QState *ensureLanguage = new QState();
    QState *ensureConfig = new QState();
    QState *runMain = new QState();
    QFinalState *theEnd = new QFinalState{};

    connect(ensureLanguage, &QState::entered, languageEnsurer,
            &LanguageEnsurer::findLanguage);
    ensureLanguage->addTransition(languageEnsurer,
                                  &LanguageEnsurer::languageSet, ensureConfig);
    ensureLanguage->addTransition(
        languageEnsurer, &LanguageEnsurer::languageChoiceAborted, theEnd);

    connect(ensureConfig, &QState::entered, configEnsurer,
            &ConfigEnsurer::ensureConfig);
    ensureConfig->addTransition(configEnsurer, &ConfigEnsurer::haveConfig,
                                runMain);
    ensureConfig->addTransition(configEnsurer, &ConfigEnsurer::configAborted,
                                theEnd);

    connect(runMain, &QState::entered, this, &ZusiLauncher ::showMainform);

    machine.addState(ensureLanguage);
    machine.addState(ensureConfig);
    machine.addState(runMain);
    machine.addState(theEnd);

    machine.setInitialState(ensureLanguage);

    machine.start();
  }

  ~ZusiLauncher() {
    if (fahrtenschreiber) {
      fahrtenschreiber->stop();
    }
  }

 private:
  void startFahrtenschreiber() {
    QSettings settings;
    if (!settings.value("config/zusiFahrtenschreiber").toBool()) {
      return;
    }

    auto err = Fahrtenschreiber::Fahrtenschreiber::initDb();
    if (err.type() != QSqlError::NoError) {
      QMessageBox msg{"Fehler bei Initialisierung des Fahrtenschreibers",
                      err.text(),
                      QMessageBox::Critical,
                      QMessageBox::Ok,
                      QMessageBox::NoButton,
                      QMessageBox::NoButton};
      msg.exec();
      return;
    }

    fahrtenschreiber = std::make_unique<Fahrtenschreiber::Fahrtenschreiber>(
        settings.value("config/zusiTcpHost").toString(),
        settings.value("config/zusiTcpPort").toInt());

    w->setFahrtenschreiber(fahrtenschreiber.get());
    fahrtenschreiber->start(w.get());
  }

 private slots:
  void showMainform() {
    QSettings settings;
    const QString rootPath{settings.value("config/zusiDataPath").toString()};

    TrainRating::openDB();

    // We have to create this after language was set
    w = std::make_unique<LauncherWindow>();
    w->setRootPath(rootPath);
    w->show();
    qApp->processEvents();

    MicroBench cachebench{"Cache Load"};
    QDir dir{};
    dir.mkpath(
        QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    QFile cacheFile{
        QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) +
        "/zusicache.dat"};
    cache = std::make_unique<ZusiCache>(rootPath, cacheFile);
    auto cacheduration = cachebench.done();

    globalZugCache = cache->zugCache();
    globalFahrzeugCache = cache->fahrzeugCache();

    model.setRootPath(rootPath + "Timtables/");
    model.setZugCache(globalZugCache);

    MicroBench bench{"Initial Load"};
    auto root = model.getRootItem();

    FahrplanParser p{rootPath, *globalZugCache};
    FahrplanFinder f{};
    f.find(QDir(rootPath + "Timetables/"));

    for (auto &&fd : f.getList()) {
      root->addFahrplan(p.read(fd));
    }

    model.loadAllZuege(true);
    cache->persist();
    double duration = bench.done();

    w->setModel(&model);
    w->showInitDuration(cacheduration + duration);

    startFahrtenschreiber();
  }

 private:
  QStateMachine machine{};
  std::unique_ptr<LauncherWindow> w{nullptr};
  std::unique_ptr<Fahrtenschreiber::Fahrtenschreiber> fahrtenschreiber{nullptr};
  FahrplanItemModel model{};
  std::unique_ptr<ZusiCache> cache{nullptr};  // TODO: no pointer
};

#include "main.moc"

int main(int argc, char *argv[]) {
  QApplication a{argc, argv};

  ZusiLauncher launcher{};

  a.exec();
}
