#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "search/searchoption.h"

#include <QApplication>
#include <QLayout>
#include <QWidget>

#include "fahrplantree.h"
#include "fahrzeugcache.h"
#include "zugcache.h"

ZugCache *globalZugCache{};
FahrzeugCache *globalFahrzeugCache{};

class AppTestEnvironment {
  int argc{0};
  char *argv[1]{const_cast<char *>("")};
  QApplication a{argc, argv};
  ZugCache zugCache{""};
  FahrzeugCache fahrzeugCache{""};

 public:
  AppTestEnvironment() {
    globalZugCache = &zugCache;
    globalFahrzeugCache = &fahrzeugCache;
  }

  ~AppTestEnvironment() {
    globalZugCache = nullptr;
    globalFahrzeugCache = nullptr;
  }
};

unsigned int Factorial(unsigned int number) {
  return number <= 1 ? number : Factorial(number - 1) * number;
}

TEMPLATE_TEST_CASE("SearchOptions returns a QWidget xor a QLayout",
                   "[SearchOption]", SearchZugType, SearchZugnummer,
                   SearchEngineType, SearchDuration, SearchBetriebsstelle,
                   SearchEngineAge, SearchRating) {
  AppTestEnvironment env{};

  TestType so{nullptr};

  auto widget = std::unique_ptr<QWidget>(so.widget());
  auto layout = std::unique_ptr<QLayout>(so.layout());

  // widget XOR layout
  REQUIRE(((widget && !layout) || (!widget && layout)));
}

TEST_CASE("SearchZugType's widget is a QComboBox ",
          "[ImplemenationDetail][SearchOption][SearchZugType]") {
  /* This actually is an implementation detail, but useful to verify for later
   * tests */
  AppTestEnvironment env{};
  SearchZugType zt{nullptr};

  std::unique_ptr<QComboBox> box{dynamic_cast<QComboBox *>(zt.widget())};
  REQUIRE(box);
}

TEST_CASE("SearchZugType defaults to Reisezug and matches accordingly",
          "[ImplemenationDetail][SearchOption][SearchZugType]") {
  AppTestEnvironment env{};

  SearchZugType zt{nullptr};

  Zug z1{"filename",     false,    "T", "1234", Zug::Reisezug,
         "Start - Ende", "Gruppe", {},  {}};
  Zug z2{"filename",     false,    "T", "1234", Zug::Gueterzug,
         "Start - Ende", "Gruppe", {},  {}};
  FahrplanTreeItemZug zi1{&z1, nullptr};
  FahrplanTreeItemZug zi2{&z2, nullptr};

  REQUIRE(zt.match(&zi1));
  REQUIRE(!zt.match(&zi2));
}

TEST_CASE("SearchZugType finds Gueterzuege",
          "[ImplemenationDetail][SearchOption][SearchZugType]") {
  AppTestEnvironment env{};

  SearchZugType zt{nullptr};

  Zug z1{"filename",     false,    "T", "1234", Zug::Reisezug,
         "Start - Ende", "Gruppe", {},  {}};
  Zug z2{"filename",     false,    "T", "1234", Zug::Gueterzug,
         "Start - Ende", "Gruppe", {},  {}};
  FahrplanTreeItemZug zi1{&z1, nullptr};
  FahrplanTreeItemZug zi2{&z2, nullptr};

  std::unique_ptr<QComboBox> box{dynamic_cast<QComboBox *>(zt.widget())};
  box->setCurrentIndex(1);

  REQUIRE(!zt.match(&zi1));
  REQUIRE(zt.match(&zi2));
}

TEST_CASE(
    "SearchZugType finds Reisezuege after temporarily picking Gueterzuege",
    "[ImplemenationDetail][SearchOption][SearchZugType]") {
  AppTestEnvironment env{};

  SearchZugType zt{nullptr};

  Zug z1{"filename",     false,    "T", "1234", Zug::Reisezug,
         "Start - Ende", "Gruppe", {},  {}};
  Zug z2{"filename",     false,    "T", "1234", Zug::Gueterzug,
         "Start - Ende", "Gruppe", {},  {}};
  FahrplanTreeItemZug zi1{&z1, nullptr};
  FahrplanTreeItemZug zi2{&z2, nullptr};

  std::unique_ptr<QComboBox> box{dynamic_cast<QComboBox *>(zt.widget())};
  box->setCurrentIndex(1);
  box->setCurrentIndex(0);

  REQUIRE(zt.match(&zi1));
  REQUIRE(!zt.match(&zi2));
}
