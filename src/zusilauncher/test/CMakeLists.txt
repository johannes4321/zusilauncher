project(zusilaunchertest)

#find_package(Catch2 REQUIRED)

add_executable(test-search searchtest.cpp)
target_include_directories(test-search PRIVATE ../../../lib/catch2/single_include/catch2)
target_link_libraries(test-search zusilauncherlib Catch2::Catch2)

include(CTest)
include(../../../lib/catch2/contrib/Catch.cmake)
catch_discover_tests(test-search)
#ParseAndAddCatchTests(test-search)
