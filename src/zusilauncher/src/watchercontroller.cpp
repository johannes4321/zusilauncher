#include "watchercontroller.h"
#include <QThread>
#include "Zusi3TCP.h"
#include "launcherwindow.h"

// std::shared_ptr<zusi::Socket> WatcherOps::getSocket() {
/*
try {
auto sock = std::make_shared<BoostAsioSyncSocket>("192.168.2.25", "1436");
return sock;
} catch (...) {
std::cerr << "BoostAsio Threw!\n" << std::endl;
throw;
}
*/
// auto *sock = new PosixSocket{"192.168.2.25", 1436};// ZusiQSocket{};
/*if (!sock->connect()) {//"192.168.2.25", 1436)) {
    throw std::runtime_error{"Not connected"};
}*/
//}

void WatcherController::start(const QString& host, int port,
                              LauncherWindow* launcher) {
  sig.moveToThread(&watchThread);
  connect(this, &WatcherController::startWatcher, &sig, &WatcherOps::doWork);
  connect(&sig, &WatcherOps::newStatus, launcher,
          &LauncherWindow::updateStatus);
  connect(&sig, &WatcherOps::connected, launcher,
          &LauncherWindow::zusiConnected);
  connect(&sig, &WatcherOps::disconnected, launcher,
          &LauncherWindow::zusiDisconnected);
  watchThread.start();
  emit startWatcher(host, port);
}
