#include "searchwidget.h"
#include "fahrzeug.h"
#include "fahrzeugcache.h"
#include "iconprovider.h"
#include "microbench.h"
#include "search/searchoption.h"
#include "trainrating.h"
#include "ui_searchwidget.h"

#include <QMenu>

extern FahrzeugCache *globalFahrzeugCache;
extern ZugCache *globalZugCache;

static QIcon emptyIcon{};

namespace {
class ResultItemModel : public QAbstractTableModel {
 public:
  ResultItemModel(std::vector<FahrplanTreeItemZug *> &&zugliste)
      : QAbstractTableModel(), zugliste{zugliste} {}

  int columnCount(const QModelIndex & /*parent*/ = {}) const override {
    return 2;
  }

  int rowCount(const QModelIndex & /*parent*/ = {}) const override {
    return static_cast<int>(zugliste.size());
  }

  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override {
    auto &zug = zugliste.at(static_cast<std::size_t>(index.row()));
    return zug->getData(index.column(), role);
  }

  const FahrplanTreeItemZug *getZug(int i) const {
    return zugliste.at(static_cast<std::size_t>(i));
  }

 private:
  std::vector<FahrplanTreeItemZug *> zugliste;
};
}  // namespace

template <typename S>
void addSearchOption(QMenu *menu, QObject *owner, QFormLayout *layout,
                     std::vector<SearchOption *> &options) {
  auto *action = menu->addAction(S::label());
  QObject::connect(action, &QAction::triggered, [layout, owner, &options]() {
    auto *search = new S{owner};

    auto widget = search->widget();
    if (widget) {
      layout->addRow(S::label(), widget);
    } else {
      layout->addRow(S::label(), search->layout());
    }
    options.push_back(search);
  });
}

SearchWidget::SearchWidget(QWidget *parent)
    : QWidget{parent}, ui{new Ui::SearchWidget} {
  ui->setupUi(this);

  connect(ui->searchButton, &QPushButton::clicked, this,
          &SearchWidget::doSearch);

  auto *layout = dynamic_cast<QFormLayout *>(ui->groupBox->layout());

  auto *menu = new QMenu{};
  addSearchOption<SearchZugType>(menu, this, layout, options);
  addSearchOption<SearchZugnummer>(menu, this, layout, options);
  addSearchOption<SearchEngineType>(menu, this, layout, options);
  addSearchOption<SearchDuration>(menu, this, layout, options);
  addSearchOption<SearchBetriebsstelle>(menu, this, layout, options);
  addSearchOption<SearchEngineAge>(menu, this, layout, options);
  addSearchOption<SearchRating>(menu, this, layout, options);
  addSearchOption<SearchZDA>(menu, this, layout, options);
  ui->addSearchButton->setMenu(menu);

  connect(ui->resetButton, &QPushButton::clicked, this,
          &SearchWidget::resetSearch);
}

SearchWidget::~SearchWidget() { delete ui; }

void SearchWidget::pickFahrplan(const QModelIndex &index,
                                const QModelIndex & /*previous*/) {
  if (!index.isValid()) {
    return;
  }

  emit zugSelected(
      static_cast<const ResultItemModel *>(index.model())->getZug(index.row()));
}

void SearchWidget::setModel(FahrplanItemModel *model) {
  this->model = model;
  emit modelSet(model);
  ui->searchButton->setEnabled(true);
}

void SearchWidget::doSearch() {
  Search s{model->getRootItem()};

  bool deko = ui->checkBox->isChecked();

  s.addPredicate([deko](const FahrplanTreeItemZug *tr) {
    return deko || !tr->getZug().isDeko();
  });

  for (const SearchOption *o : options) {
    s.addPredicate(std::bind(&SearchOption::match, o, std::placeholders::_1));
  }

  MicroBench bench{"Search"};
  auto zugliste = s.search();
  bench.done();

  ui->resultView->setModel(new ResultItemModel(std::move(zugliste)));
  connect(ui->resultView->selectionModel(),
          &QItemSelectionModel::currentChanged, this,
          &SearchWidget::pickFahrplan);
}

void SearchWidget::resetSearch() {
  for (auto &o : options) {
    // This will crash due to mismatch, assuming parent-child-relationship will
    // clean: free(option);
    o = nullptr;

    // Row zero is "Dekozüge" that one we keep
    ui->formLayout->removeRow(1);
  }
  options.clear();
  ui->resultView->setModel(nullptr);
}
