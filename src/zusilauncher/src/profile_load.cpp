#include <algorithm>
#include <iostream>

#include <QApplication>
#include <QDataStream>
#include <QFile>
#include <QStandardPaths>

#include "fahrplanfinder.h"
#include "fahrplanitemmodel.h"
#include "fahrplanparser.h"
#include "fahrzeugcache.h"
#include "microbench.h"
#include "zugcache.h"
#include "zusicache.h"

ZugCache *globalZugCache;
FahrzeugCache *globalFahrzeugCache;

int main(int argc, char *argv[]) {
  QString rootPath = R"(c:\Program Files (x86)\Zusi3\_ZusiData\)";
  if (argc == 2 && std::string{"--help"} == argv[1]) {
    std::cerr << "Usage:\n\t" << argv[0] << " zusiroot\n";
    return 1;
  } else if (argc == 2) {
    rootPath = QString::fromUtf8(argv[1]) + '/';
  }

  QApplication a(argc, argv);
  QCoreApplication::setOrganizationName("Johannnes");
  QCoreApplication::setOrganizationDomain("schlueters.de");
  QCoreApplication::setApplicationName("ZusiLauncher");

  QDir dir{};
  dir.mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
  QFile cacheFile{
      QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) +
      "/zusicache.dat"};
  ZusiCache cache{rootPath, cacheFile};

  globalZugCache = cache.zugCache();
  globalFahrzeugCache = cache.fahrzeugCache();

  FahrplanItemModel model{};
  model.setRootPath(rootPath + "Timetables/");
  model.setZugCache(globalZugCache);
  auto root = model.getRootItem();

  MicroBench bench{"Initial Load"};
  FahrplanParser p{rootPath, *globalZugCache};
  FahrplanFinder f;
  f.find(QDir(rootPath + "Timetables/"));

  for (auto &&fd : f.getList()) {
    root->addFahrplan(p.read(fd));
  }
  bench.done();

  MicroBench bench2{"Load All"};
  model.loadAllZuege(false);
  bench2.done();
}
