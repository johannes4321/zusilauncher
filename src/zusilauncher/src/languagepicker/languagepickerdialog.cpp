#include "languagepickerdialog.h"
#include "ui_languagepickerdialog.h"

LanguagePickerDialog::LanguagePickerDialog(QWidget *parent)
    : QDialog(parent), ui(new Ui::LanguagePickerDialog) {
  ui->setupUi(this);

  connect(this, &QDialog::accepted, this,
          &LanguagePickerDialog::choiceAccepted);

  ui->languageComboBox->addItem("Deutsch", "de");
  ui->languageComboBox->addItem("English", "en");
}

LanguagePickerDialog::~LanguagePickerDialog() { delete ui; }

void LanguagePickerDialog::choiceAccepted() {
  emit accepted(ui->languageComboBox->currentData().toString());
}
