#ifndef LANGUAGEPICKERDIALOG_H
#define LANGUAGEPICKERDIALOG_H

#include <QDialog>

namespace Ui {
class LanguagePickerDialog;
}

class LanguagePickerDialog : public QDialog {
  Q_OBJECT

 public:
  explicit LanguagePickerDialog(QWidget *parent = nullptr);
  ~LanguagePickerDialog();

 signals:
  void accepted(const QString &language);

 private slots:
  void choiceAccepted();

 private:
  Ui::LanguagePickerDialog *ui;
};

#endif  // LANGUAGEPICKERDIALOG_H
