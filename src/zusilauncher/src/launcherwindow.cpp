#include "launcherwindow.h"
#include <QListWidget>
#include <QMessageBox>
#include <QSortFilterProxyModel>
#include <QThread>
#include <QUrl>
#include "fahrplanitemmodel.h"
#include "trainwidget.h"
#include "ui_launcherwindow.h"

#define APP_VERSION "0.8.1"

extern ZugCache *globalZugCache;

void LauncherWindow::analyze() {
  QDialog *dialog = new QDialog{this};
  QGridLayout *layout = new QGridLayout{dialog};
  dialog->setLayout(layout);
  QLabel *label = new QLabel{dialog};
  label->setWordWrap(true);
  if (model) {
    label->setText(model->analyze());
  } else {
    label->setText(tr("Keine Daten geladen (model is NULL)"));
  }
  dialog->layout()->addWidget(label);
  dialog->exec();
}

LauncherWindow::LauncherWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::LauncherWindow) {
  ui->setupUi(this);

  web = new QWebEngineView(ui->browserPage);
  ui->browserPage->layout()->addWidget(web);

  ui->treeView->sortByColumn(0, Qt::AscendingOrder);
  ui->stackedWidget->hide();
  ui->stackedWidget->setCurrentWidget(ui->startPage);

  connect(ui->actionDatenbestand_analysieren, &QAction::triggered, this,
          &LauncherWindow::analyze);
  connect(ui->action_ueber_Qt, &QAction::triggered,
          [this]() { QMessageBox::aboutQt(this); });
  connect(ui->action_ueber_ZusiLauncher, &QAction::triggered, [this]() {
    QMessageBox::about(this, "ZusiLauncher",
                       "ZusiLauncher " APP_VERSION
                       "<br><br>Copyright &copy;2017-2021 Johannes Schlüter");
  });

  connect(ui->searchTab, &SearchWidget::zugSelected, this,
          &LauncherWindow::zugSelectedFromSearch);

  // ui->menuEntwickleroptionen->hide();

  ui->tabWidget->setTabEnabled(2, false);

#ifdef TODO_123456
  // TODO: We need a FahrplanTreeItem here to be able to render the details
  connect(ui->fahrtenschreiberTab,
          &Fahrtenschreiber::FahrtenschreiberWidget::zugSelected,
          [this](const QString &filename) {
            auto zug = globalZugCache->getEntry(filename);
            ui->stackedWidget->show();
            renderTrainPage(&zug);
          });
#endif

  ui->trainPage->setFahrtenschreiber(nullptr);
  ui->statusBar->showMessage(tr("Lade ..."));
}

void LauncherWindow::setRootPath(const QString &rootPath) {
  this->rootPath = rootPath;
}

void LauncherWindow::setFahrtenschreiber(
    Fahrtenschreiber::Fahrtenschreiber *fahrtenschreiber) {
  this->fahrtenschreiber = fahrtenschreiber;

  ui->trainPage->setFahrtenschreiber(fahrtenschreiber);
  ui->fahrtenschreiberTab->fahrtenschreiber(fahrtenschreiber);
  ui->tabWidget->setTabEnabled(2, true);

  // ui->menuEntwickleroptionen->show();
  connect(ui->actionProtokoll, &QAction::triggered, fahrtenschreiber,
          &Fahrtenschreiber::Fahrtenschreiber::showLog);
}

void LauncherWindow::updateStatus(const QString &status) {
  ui->statusBar->showMessage(status, 5000);
}

void LauncherWindow::zusiConnected(const QString &zusiVersion) {
  if (!zusiVersionLabel) {
    zusiVersionLabel = new QLabel(this);
    ui->statusBar->insertPermanentWidget(0, zusiVersionLabel);
  }
  zusiVersionLabel->setText(QString("Zusi ") + zusiVersion);
}
void LauncherWindow::zusiDisconnected() {
  if (zusiVersionLabel) {
    zusiVersionLabel->setText(tr("Zusi beendet"));
  }
}

void LauncherWindow::renderPlanPage(const FahrplanTreeItemPlan *item) {
  ui->stackedWidget->setCurrentWidget(ui->browserPage);
  web->load(QUrl::fromLocalFile(rootPath + item->getPlan()->begruessungsDatei));
}

void LauncherWindow::renderTrainPage(const FahrplanTreeItemZug *item) {
  ui->stackedWidget->setCurrentWidget(ui->trainPage);
  ui->trainPage->setZug(item);
}

void LauncherWindow::zugSelectedFromSearch(const FahrplanTreeItemZug *zug) {
  ui->stackedWidget->show();
  renderTrainPage(zug);
}

void LauncherWindow::pickFahrplan(const QModelIndex &index,
                                  const QModelIndex &previous) {
  ui->stackedWidget->show();

  const QModelIndex &indexe =
      dynamic_cast<const QSortFilterProxyModel *>(index.model())
          ->mapToSource(index);
  FahrplanTreeItem *item =
      static_cast<FahrplanTreeItem *>(indexe.internalPointer());
  switch (item->getType()) {
    case FahrplanTreeItem::ROOT:
    case FahrplanTreeItem::FILE_PATH:
      ui->stackedWidget->setCurrentWidget(ui->startPage);
      ui->stackedWidget->hide();
      break;
    case FahrplanTreeItem::GRUPPE:
      item = item->getParentItem();
      [[fallthrough]];
    case FahrplanTreeItem::PLAN:
      if (previous.internalPointer() != item)
        renderPlanPage(dynamic_cast<FahrplanTreeItemPlan *>(item));
      break;
    case FahrplanTreeItem::TRAIN:
      renderTrainPage(dynamic_cast<FahrplanTreeItemZug *>(item));
      break;
  }
}

void LauncherWindow::showInitDuration(double duration) {
  ui->statusBar->showMessage(tr("Init time: ") + QString::number(duration),
                             5000);
}

void LauncherWindow::setModel(FahrplanItemModel *model) {
  this->model = model;
  ui->searchTab->setModel(model);
  QSortFilterProxyModel *proxyModel = new QSortFilterProxyModel(model);
  proxyModel->setSourceModel(model);
  proxyModel->setSortCaseSensitivity(Qt::CaseInsensitive);
  proxyModel->setSortLocaleAware(true);
  ui->treeView->setModel(proxyModel);
  ui->treeView->setColumnWidth(0, ui->treeView->width() / 3);
  connect(model, &FahrplanItemModel::zugSummariesLoaded,
          [=](const QString &filename, double duration) {
            this->ui->statusBar->showMessage(tr("Fahrplan geladen: ") +
                                             filename + " " +
                                             QString::number(duration));
          });
  connect(ui->treeView->selectionModel(), &QItemSelectionModel::currentChanged,
          this, &LauncherWindow::pickFahrplan);
}

LauncherWindow::~LauncherWindow() {}
