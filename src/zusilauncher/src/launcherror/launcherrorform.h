#ifndef LAUNCHERRORFORM_H
#define LAUNCHERRORFORM_H

#include <memory>

#include <QWidget>

namespace Ui {
class LaunchErrorForm;
}

// to make libstdc++ happy ...
#include "ui_launcherrorform.h"

class LaunchErrorForm : public QWidget {
  Q_OBJECT

 public:
  explicit LaunchErrorForm(QWidget *parent = nullptr);
  LaunchErrorForm(const QString &filename, const QString &description,
                  QWidget *parent = nullptr);
  ~LaunchErrorForm() = default;

  void setFilename(const QString &value);

  void setDescription(const QString &value);

 private:
  std::unique_ptr<Ui::LaunchErrorForm> ui;
  QString filename;
  QString description;

 private slots:
  void openInExplorer();
  void runFile();
};

#endif  // LAUNCHERRORFORM_H
