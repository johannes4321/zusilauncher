#include "launcherrorform.h"
#include "ui_launcherrorform.h"

#include <QDesktopServices>
#include <QDir>
#include <QMessageBox>
#include <QProcess>
#include <QUrl>

LaunchErrorForm::LaunchErrorForm(QWidget *parent)
    : QWidget{parent},
      ui{std::make_unique<Ui::LaunchErrorForm>()},
      filename{},
      description{} {
  ui->setupUi(this);

  connect(ui->explorerButton, &QPushButton::clicked, this,
          &LaunchErrorForm::openInExplorer);
  connect(ui->retryButton, &QPushButton::clicked, this,
          &LaunchErrorForm::runFile);
}

LaunchErrorForm::LaunchErrorForm(const QString &filename,
                                 const QString &description, QWidget *parent)
    : LaunchErrorForm{parent} {
  setFilename(filename);
  setDescription(description);
}

void LaunchErrorForm::setFilename(const QString &value) {
  filename = value;
  ui->filenameEdit->setText(filename);
}

void LaunchErrorForm::setDescription(const QString &value) {
  description = value;
  ui->descriptionLabel->setText(description);
}

void LaunchErrorForm::openInExplorer() {
  QStringList args;

  args << "/select," << QDir::toNativeSeparators(filename);

  QProcess *process = new QProcess(this);
  if (!process->startDetached("explorer.exe", args)) {
    QMessageBox msgBox;
    msgBox.setText(tr("Explorer konnte nicht gestartet werden."));
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.exec();
  }
}

void LaunchErrorForm::runFile() {
  auto url = QUrl::fromLocalFile(filename + "fdsf");
  if (!QDesktopServices::openUrl(url)) {
    QMessageBox msgBox;
    msgBox.setText(tr("Das ging wohl wieder schief ..."));
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.exec();
  } else {
    this->close();
  }
}
