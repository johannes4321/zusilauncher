#ifndef SEARCHOPTION_H
#define SEARCHOPTION_H

#include <QObject>

class FahrplanItemModel;
class FahrplanTreeItemZug;
class Zug;

class QLayout;

class SearchOption : public QObject {
  Q_OBJECT
 protected:
  explicit SearchOption(QObject *parent = nullptr);

 public:
  virtual ~SearchOption() = default;

  // This is expected in all dericved types:
  // TODO: Is there a better way to keep the label as part of the class, but not
  // bound to an instance
  //
  // static const QString *label() const = 0;

  QWidget *widget(QWidget *parent = nullptr);
  QLayout *layout(QWidget *parent = nullptr);
  bool match(const FahrplanTreeItemZug *item) const;

 private:
  virtual QWidget *createWidget(QWidget * /*parent*/ = nullptr) {
    return nullptr;
  }
  virtual QLayout *createLayout(QWidget * /*parent*/ = nullptr) {
    return nullptr;
  }
  virtual bool doMatch(const FahrplanTreeItemZug *item) const = 0;
};

#include "zug.h"

class SearchZugOption : public SearchOption {
  Q_OBJECT
 public:
 protected:
  explicit SearchZugOption(QObject *parent = nullptr);

 public:
  virtual ~SearchZugOption() = default;

  bool doMatch(const FahrplanTreeItemZug *item) const final override;
  virtual bool doMatch(const Zug &zug) const = 0;
};

class SearchZugType final : public SearchZugOption {
  Q_OBJECT;

 public:
  static QString label();

  SearchZugType(QObject *parent) : SearchZugOption{parent} {}
  ~SearchZugType() = default;

  QWidget *createWidget(QWidget *parent = nullptr) override;
  bool doMatch(const Zug &zug) const override;

 private:
  Zug::Zugtyp type{Zug::Reisezug};
};

class SearchZugnummer final : public SearchZugOption {
  Q_OBJECT;

 public:
  static QString label();

  SearchZugnummer(QObject *parent) : SearchZugOption{parent} {}
  ~SearchZugnummer() = default;

  QLayout *createLayout(QWidget *parent = nullptr) override;
  bool doMatch(const Zug &zug) const override;

 private:
  QString gattung;
  QString nummer;
};

class QComboBox;

class SearchEngineType final : public SearchZugOption {
  Q_OBJECT
 public:
  static QString label();

  explicit SearchEngineType(QObject *parent = nullptr)
      : SearchZugOption{parent} {}
  QLayout *createLayout(QWidget *parent = nullptr) override;
  bool doMatch(const Zug &zug) const override;

 private slots:
  void baureiheSelected(const QString &br);

 private:
  QComboBox *variantenBox{nullptr};
  QString baureihe{""};
  QString variante{""};
};

class SearchDuration final : public SearchZugOption {
  Q_OBJECT
 public:
  static QString label();

  explicit SearchDuration(QObject *parent = nullptr)
      : SearchZugOption{parent} {}
  QLayout *createLayout(QWidget *parent = nullptr) override;
  bool doMatch(const Zug &zug) const override;

 private:
  enum Direction { AT_LEAST, UP_TO };
  Direction direction{AT_LEAST};
  int duration;
};

class SearchBetriebsstelle final : public SearchZugOption {
  Q_OBJECT
 public:
  static const QString label();

  explicit SearchBetriebsstelle(QObject *parent = nullptr);
  QWidget *createWidget(QWidget *parent = nullptr) override;
  bool doMatch(const Zug &zug) const override;

 private slots:
  void textChanged(const QString &str);

 private:
  QString m_betriebstelle{};
};

class SearchEngineAge final : public SearchZugOption {
  Q_OBJECT
 public:
  static QString label();

  explicit SearchEngineAge(QObject *parent = nullptr)
      : SearchZugOption{parent} {}
  QLayout *createLayout(QWidget *parent = nullptr) override;
  bool doMatch(const Zug &zug) const override;

 private:
  enum Direction { AT_LEAST, UP_TO };
  Direction direction{AT_LEAST};
  QDate date{};
};

#include "trainrating.h"

class SearchRating final : public SearchZugOption {
  Q_OBJECT
 public:
  static QString label();

  explicit SearchRating(QObject *parent = nullptr) : SearchZugOption{parent} {}
  QWidget *createWidget(QWidget *parent = nullptr) override;
  bool doMatch(const Zug &zug) const override;

 private:
  TrainRating ratingEngine{};
  QChar rating{'J'};
};

class SearchZDA final : public SearchZugOption {
  Q_OBJECT
 public:
  static QString label();

  explicit SearchZDA(QObject *parent = nullptr) : SearchZugOption{parent} {}
  QWidget *createWidget(QWidget *parent = nullptr) override;
  bool doMatch(const Zug &zug) const override;

 private:
  bool required;
};

#endif  // SEARCHOPTION_H
