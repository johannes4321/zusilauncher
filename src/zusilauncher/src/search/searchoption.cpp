#include "searchoption.h"

#include <algorithm>

#include "fahrplanitemmodel.h"

SearchOption::SearchOption(QObject *parent) : QObject{parent} {}

QWidget *SearchOption::widget(QWidget *parent) { return createWidget(parent); }
bool SearchOption::match(const FahrplanTreeItemZug *item) const {
  return doMatch(item);
}

QLayout *SearchOption::layout(QWidget *parent) { return createLayout(parent); }

SearchZugOption::SearchZugOption(QObject *parent) : SearchOption{parent} {}

bool SearchZugOption::doMatch(const FahrplanTreeItemZug *item) const {
  return doMatch(item->getZug());
}

SearchBetriebsstelle::SearchBetriebsstelle(QObject *parent)
    : SearchZugOption{parent} {}

const QString SearchBetriebsstelle::label() {
  return tr("Durchfahrene Betriebsstelle");
}

#include <QBoxLayout>
#include <QComboBox>
#include <QDateEdit>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include "iconprovider.h"

extern ZugCache *globalZugCache;

QString SearchZugType::label() { return tr("Zugart"); }

QWidget *SearchZugType::createWidget(QWidget *parent) {
  auto *w = new QComboBox{parent};

  w->addItem(tr("Reisezug"));
  w->addItem(tr("Güterzug"));

  connect(w, QOverload<int>::of(&QComboBox::currentIndexChanged),
          [this](int index) {
            type = index == 0 ? Zug::Reisezug : Zug::Gueterzug;
          });

  return w;
}
bool SearchZugType::doMatch(const Zug &zug) const {
  return type == zug.getZugtyp();
}

QString SearchZugnummer::label() { return tr("Zugnummer"); }

QLayout *SearchZugnummer::createLayout(QWidget *parent) {
  static IconProvider
      iconprovider{};  // TODO: Inject this so we have single instance only
  auto *layout = new QBoxLayout{QBoxLayout::LeftToRight, parent};

  auto *gattungsBox = new QComboBox{};
  auto *numberBox = new QLineEdit{};

  QStringList gattungen{""};

  if (globalZugCache) {
    std::for_each(globalZugCache->begin(), globalZugCache->end(),
                  [&gattungen](auto &file_and_zug) {
                    auto &[filename, zug] = file_and_zug;
                    if (zug.isDeko()) {
                      return;
                    }

                    gattungen.append(zug.getGattung());
                  });

    gattungen.sort();
    gattungen.removeDuplicates();

    for (auto &gattung : gattungen) {
      gattungsBox->addItem(iconprovider(gattung), gattung);
    }
  }

  connect(gattungsBox,
          QOverload<const QString &>::of(&QComboBox::currentIndexChanged),
          [this](const QString &gattung) { this->gattung = gattung; });
  connect(numberBox, &QLineEdit::textChanged,
          [this](const QString &nr) { nummer = nr; });

  layout->addWidget(gattungsBox);
  layout->addWidget(numberBox);

  return layout;
}

bool SearchZugnummer::doMatch(const Zug &zug) const {
  return (gattung.isEmpty() || zug.getGattung() == gattung) &&
         zug.getNummer().contains(nummer);
}

QString SearchEngineType::label() { return tr("Baureihe"); }

QLayout *SearchEngineType::createLayout(QWidget *parent) {
  auto *layout = new QBoxLayout{QBoxLayout::LeftToRight, parent};

  auto *baureiheBox = new QComboBox{};
  variantenBox = new QComboBox{};

  QStringList baureihen{};

  if (globalZugCache) {
    std::for_each(globalZugCache->begin(), globalZugCache->end(),
                  [&baureihen](auto &file_and_zug) {
                    auto &[filename, zug] = file_and_zug;
                    if (zug.isDeko()) {
                      return;
                    }

                    auto &fahrzeuge = zug.getFahrzeuge();
                    if (!fahrzeuge.length()) {
                      return;
                    }

                    baureihen.append(fahrzeuge.begin()->BR);
                  });

    baureihen.sort();
    baureihen.removeDuplicates();
    baureiheBox->addItems(baureihen);
  }

  connect(baureiheBox,
          QOverload<const QString &>::of(&QComboBox::currentIndexChanged), this,
          &SearchEngineType::baureiheSelected);

  connect(variantenBox,
          QOverload<const QString &>::of(&QComboBox::currentIndexChanged),
          [this](const QString &v) { variante = v; });

  layout->addWidget(baureiheBox);
  layout->addWidget(variantenBox);

  return layout;
}

void SearchEngineType::baureiheSelected(const QString &br) {
  baureihe = br;

  variantenBox->clear();

  QStringList varianten = std::accumulate(
      globalZugCache->begin(), globalZugCache->end(), QStringList{""},
      [&br](QStringList &varianten, auto &file_and_zug) {
        auto &[filename, zug] = file_and_zug;

        auto &frzge = zug.getFahrzeuge();
        if (frzge.length()) {
          auto frzg = frzge.first();
          if (frzg.BR == br) {
            varianten.append(frzg.Beschreibung);
          }
        }
        return varianten;
      });

  varianten.sort();
  varianten.removeDuplicates();

  variantenBox->addItems(varianten);
}

bool SearchEngineType::doMatch(const Zug &zug) const {
  const auto &fzg = zug.getFahrzeuge();
  if (!fzg.length()) {
    return false;
  }

  const auto &triebfzg = fzg.begin();

  return baureihe == triebfzg->BR &&
         (variante.isEmpty() || variante == triebfzg->Beschreibung);
}

QString SearchDuration::label() { return tr("Fahrtdauer"); }

QLayout *SearchDuration::createLayout(QWidget *parent) {
  auto *layout = new QBoxLayout{QBoxLayout::LeftToRight, parent};

  auto *directionBox = new QComboBox{};
  auto *durationSpin = new QSpinBox{};
  auto *unitLable = new QLabel{tr("Minuten")};

  directionBox->addItem(tr("mindestens"));
  directionBox->addItem(tr("maximal"));

  durationSpin->setMaximum(999);

  connect(directionBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
          [this](int index) { direction = index == 0 ? AT_LEAST : UP_TO; });
  connect(durationSpin, QOverload<int>::of(&QSpinBox::valueChanged),
          [this](int val) { duration = val; });

  layout->addWidget(directionBox);
  layout->addWidget(durationSpin);
  layout->addWidget(unitLable);

  return layout;
}

bool SearchDuration::doMatch(const Zug &zug) const {
  auto fahrzeit = zug.getFahrzeit();
  auto dur = fahrzeit.hour() * 60 + fahrzeit.minute();

  switch (direction) {
    case AT_LEAST:
      return dur >= duration;
    case UP_TO:
      return dur <= duration;
  }
}

QWidget *SearchBetriebsstelle::createWidget(QWidget *parent) {
  auto l = new QLineEdit{parent};
  connect(l, &QLineEdit::textChanged, this, &SearchBetriebsstelle::textChanged);
  return l;
}

void SearchBetriebsstelle::textChanged(const QString &str) {
  m_betriebstelle = str;
}

bool SearchBetriebsstelle::doMatch(const Zug &zug) const {
  const auto &fahrplan = zug.getFahrplan();
  return std::find_if(fahrplan.begin(), fahrplan.end(),
                      [this](const auto &entry) {
                        return entry.Betrst.contains(m_betriebstelle,
                                                     Qt::CaseInsensitive);
                      }) != fahrplan.end();
}

QString SearchEngineAge::label() { return tr("Ersteinsatz der Lok"); }

QLayout *SearchEngineAge::createLayout(QWidget *parent) {
  auto *layout = new QBoxLayout{QBoxLayout::LeftToRight, parent};

  auto *directionBox = new QComboBox{};
  auto *dateEdit = new QDateEdit{};

  directionBox->addItem(tr("nach"));
  directionBox->addItem(tr("vor"));

  dateEdit->setDate(QDate{2000, 1, 1});
  dateEdit->setDisplayFormat("dd.MM.yyyy");

  connect(directionBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
          [this](int index) { direction = index == 0 ? AT_LEAST : UP_TO; });
  connect(dateEdit, &QDateEdit::dateChanged,
          [this](const QDate &d) { date = d; });

  layout->addWidget(directionBox);
  layout->addWidget(dateEdit);

  date = dateEdit->date();

  return layout;
}

bool SearchEngineAge::doMatch(const Zug &zug) const {
  const auto &frzge = zug.getFahrzeuge();
  if (!frzge.length()) {
    return false;
  }
  auto frzg = frzge.begin();
  if (!frzg->EinsatzAb.isValid()) {
    return false;
  }

  auto actual = frzg->EinsatzAb;

  switch (direction) {
    case AT_LEAST:
      return actual >= date;
    case UP_TO:
      return actual <= date;
  }
}

QString SearchRating::label() { return tr("Bewertung"); }

QWidget *SearchRating::createWidget(QWidget *parent) {
  auto *w = new QComboBox{parent};

  QFont wingdings{"wingdings", 18};
  w->setFont(wingdings);
  w->maximumSize().setWidth(60);
  w->addItem("J");
  w->addItem("K");
  w->addItem("L");

  connect(w, QOverload<const QString &>::of(&QComboBox::currentIndexChanged),
          [this](const QString &r) { rating = r.at(0); });

  return w;
}

bool SearchRating::doMatch(const Zug &zug) const {
  auto rv = static_cast<Rating>(rating.toLatin1());
  return rv == ratingEngine.getRating(zug);
}

#include <QCheckBox>

QString SearchZDA::label() { return tr("ZDA Daten (Ansagen, experimentell)"); }

QWidget *SearchZDA::createWidget(QWidget *parent) {
  auto *w = new QCheckBox{tr("ZDA Daten gewünscht"), parent};

  required = true;
  w->setChecked(true);

  connect(w, &QCheckBox::stateChanged,
          [this](int state) { required = (state == Qt::Checked); });

  return w;
}

bool SearchZDA::doMatch(const Zug &zug) const {
  return required == zug.hasZDA();
}
