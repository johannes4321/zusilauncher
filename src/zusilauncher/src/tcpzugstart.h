#ifndef TCPZUGSTART_H
#define TCPZUGSTART_H

class QString;
class Zug;

void launchZugviaTCP(const QString &host, int port, const Zug &zug, const QString &fahrplanFile);

#endif  // TCPZUGSTART_H
