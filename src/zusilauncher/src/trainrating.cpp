#include "trainrating.h"

#include "zug.h"

TrainRating::TrainRating()
    : db{QSqlDatabase::database("ratings", true)},
      deleteQuery{db},
      lookupQuery{db},
      rateQuery{db} {
  auto tables = db.tables();
  if (!tables.contains("ratings", Qt::CaseInsensitive)) {
    db.exec(
        "CREATE TABLE ratings (id INTEGER PRIMARY KEY AUTOINCREMENT, fahrplan "
        "VARCHAR(255) UNIQUE, rating char(1))");
  }

  deleteQuery.prepare("DELETE FROM ratings WHERE fahrplan = ?");
  lookupQuery.prepare("SELECT rating FROM ratings WHERE fahrplan = ?");
  rateQuery.prepare("REPLACE INTO ratings (fahrplan, rating) VALUES (?, ?)");
}

void TrainRating::openDB() {
  auto db = QSqlDatabase::addDatabase("QSQLITE", "ratings");
  db.setDatabaseName(
      QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) +
      "/ratings.sqlite");
  db.open();
}

void TrainRating::setRating(const Zug &zug, const QString &text) {
  if (text.length() == 1) {
    Rating value = static_cast<Rating>(text.at(0).toLatin1());
    setRating(zug, value);
  } else {
    unsetRating(zug);
  }
}

void TrainRating::setRating(const Zug &zug, Rating rating) {
  if (rating == Rating::NONE) {
    deleteQuery.bindValue(0, zug.getFilename());
    deleteQuery.exec();
  } else {
    rateQuery.bindValue(0, zug.getFilename());
    rateQuery.bindValue(1, QString{static_cast<char>(rating)});
    rateQuery.exec();
  }
}

[[nodiscard]] Rating TrainRating::getRating(const Zug &zug) const {
  lookupQuery.bindValue(0, zug.getFilename());
  lookupQuery.exec();
  if (!lookupQuery.next()) {
    return Rating::NONE;
  }

  auto val = lookupQuery.value(0).toString();
  if (val.length() != 1) {
    return Rating::NONE;
  }

  auto code = val.at(0).toLatin1();
  switch (code) {
    case static_cast<char>(Rating::NEUTRAL):
      return Rating::NEUTRAL;
    case static_cast<char>(Rating::NEGATIVE):
      return Rating::NEGATIVE;
    case static_cast<char>(Rating::POSITIVE):
      return Rating::POSITIVE;
    default:
      return Rating::NONE;
  }
}
