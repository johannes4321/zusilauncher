#ifndef SEARCHWIDGET_H
#define SEARCHWIDGET_H

#include <QWidget>
#include "fahrplanitemmodel.h"

class SearchOption;

namespace Ui {
class SearchWidget;
}
class SearchWidgetTest;

class SearchWidget : public QWidget {
  friend SearchWidgetTest;

  Q_OBJECT

 public:
  explicit SearchWidget(QWidget *parent = nullptr);
  ~SearchWidget();

  void setModel(FahrplanItemModel *model);

 signals:
  void modelSet(FahrplanItemModel *model);
  void zugSelected(const FahrplanTreeItemZug *zug);

 private slots:
  void doSearch();
  void resetSearch();

  void pickFahrplan(const QModelIndex &index, const QModelIndex &previous);

 private:
  Ui::SearchWidget *ui;
  FahrplanItemModel *model;
  std::vector<SearchOption *> options{};
};

#endif  // SEARCHWIDGET_H
