#ifndef REPORTFORM_H
#define REPORTFORM_H

#include <QAbstractSeries>
#include <QWidget>

namespace Fahrtenschreiber {
class Fahrtenschreiber;
namespace Ui {
class ReportForm;
}

class ReportForm : public QWidget {
  Q_OBJECT

 public:
  explicit ReportForm(QWidget *parent = nullptr);
  ~ReportForm();

  Q_PROPERTY(int id MEMBER m_id WRITE id)
  Q_PROPERTY(Fahrtenschreiber *fahrtenschreiber MEMBER m_fahrtenschreiber WRITE
                 fahrtenschreiber)

  void id(qlonglong id) {
    m_id_set = true;
    m_id = id;
    if (m_fahrtenschreiber) {
      emit dataChanged(m_id, m_fahrtenschreiber);
    }
  }
  void fahrtenschreiber(Fahrtenschreiber *fahrtenschreiber) {
    m_fahrtenschreiber = fahrtenschreiber;
    if (m_id_set) {
      emit dataChanged(m_id, m_fahrtenschreiber);
    }
  }

 private:
  Ui::ReportForm *ui;

  qlonglong m_id;
  bool m_id_set = false;
  Fahrtenschreiber *m_fahrtenschreiber = nullptr;

 signals:
  void dataChanged(qlonglong id, Fahrtenschreiber *fahrtenschreiber);

 private slots:
  void render(qlonglong id, Fahrtenschreiber *fahrtenschreiber);
};
}  // namespace Fahrtenschreiber

#endif  // REPORTFORM_H
