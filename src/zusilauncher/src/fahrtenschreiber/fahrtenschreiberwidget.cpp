#include "fahrtenschreiberwidget.h"
#include "ui_fahrtenschreiberwidget.h"

namespace Fahrtenschreiber {
FahrtenschreiberWidget::FahrtenschreiberWidget(QWidget *parent)
    : QWidget(parent),
      ui(new Ui::FahrtenschreiberWidget),
      m_fahrtenschreiber(nullptr) {
  ui->setupUi(this);
}

FahrtenschreiberWidget::~FahrtenschreiberWidget() { delete ui; }

void FahrtenschreiberWidget::fahrtenschreiber(
    Fahrtenschreiber *fahrtenschreiber) {
  m_fahrtenschreiber = fahrtenschreiber;

  if (!fahrtenschreiber) {
    return;
  }

  auto model = fahrtenschreiber->getFullReport(this);
  ui->treeView->setModel(model);

  connect(ui->treeView, &QAbstractItemView::doubleClicked,
          [fahrtenschreiber, this](const QModelIndex &idx) {
            auto model = this->ui->treeView->model();
            auto data = model->sibling(idx.row(), 0, idx).data();
            auto id = data.toLongLong();
            fahrtenschreiber->getReportForm(id)->show();
          });

  connect(ui->treeView->selectionModel(), &QItemSelectionModel ::currentChanged,
          [this](const QModelIndex &current, const QModelIndex &) {
            auto model = this->ui->treeView->model();
            auto data = model->sibling(current.row(), 2, current).data();
            emit zugSelected(data.toString());
          });
}
}  // namespace Fahrtenschreiber
