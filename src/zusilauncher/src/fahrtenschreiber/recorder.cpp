#include "recorder.h"

#include <iostream>

namespace Fahrtenschreiber {
Recorder::Recorder(QSqlDatabase *db, QObject *parent)
    : QObject(parent), newFahrtQuery{*db}, newDataQuery{*db} {
  if (newFahrtQuery.prepare(
          QLatin1String("insert into fahrten(fahrplan, starttime) values(?, "
                        "DATETIME('now'))"))) {
    std::cout << "ERR1: " << newFahrtQuery.lastError().text().toStdString();
  }

  if (newDataQuery.prepare(QLatin1String(
          "insert into fahrtdata(fahrtid, distance, speed, "
          "streckengeschwindigkeit, zugkraft, drehzahl, "
          "ingametime, sifa_hupe) values(?, ?, ?, ?, ?, ?, ?, ?)"))) {
    std::cout << "ERR3: " << newDataQuery.lastError().text().toStdString()
              << '\n';
  }
}

void Recorder::gotFahrplan(const QString &fahrplan, bool /* unused */) {
  if (fahrplan.isEmpty()) {
    return;
  }

  currentId = 0;

  newFahrtQuery.bindValue(0, fahrplan);
  if (!newFahrtQuery.exec()) {
    std::cout << "ERR2: " << newFahrtQuery.lastError().text().toStdString();
  }
  currentId = newFahrtQuery.lastInsertId().toInt();
}

template <typename T>
static inline void bindIfValue(QSqlQuery &q, int field,
                               std::optional<T> value) {
  if (value) {
    q.bindValue(field, *value);
  } else {
    q.bindValue(field, QVariant::String);
  }
}

void Recorder::stateChanged(ZusiWatcher::ZusiState state) {
  if (!currentId) {
    // No Fahrplan info
    return;
  }

  newDataQuery.bindValue(0, currentId);
  bindIfValue(newDataQuery, 1, state.distance);
  bindIfValue(newDataQuery, 2, state.speed);
  bindIfValue(newDataQuery, 3, state.streckengeschwindigkeit);
  bindIfValue(newDataQuery, 4, state.zugkraft);
  bindIfValue(newDataQuery, 5, state.drehzahl);

  if (state.time && state.date) {
    QDateTime date{QDate{1899, 1, 1},
                   QTime{static_cast<int>(state.time->h.count()),
                         static_cast<int>(state.time->m.count()),
                         static_cast<int>(state.time->s.count()), 0}};

    QDateTime date2 = date.addDays(
        static_cast<qint64>(floor(static_cast<double>(*state.date))));
    newDataQuery.bindValue(6, date2.toString("yyyy-MM-dd HH:mm:ss"));
  } else {
    newDataQuery.bindValue(6, QVariant(QVariant::String));
  }

  bindIfValue(newDataQuery, 7, state.sifa_hupe);

  if (!newDataQuery.exec()) {
    std::cout << "ERR4: " << newDataQuery.lastError().text().toStdString()
              << '\n';
  }
}
}  // namespace Fahrtenschreiber
