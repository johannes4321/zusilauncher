#include "protocolform.h"
#include "ui_protocolform.h"

#include <cmath>

#include <QFileDialog>
#include <QMessageBox>
#include <QStandardPaths>

ProtocolForm::ProtocolForm(QWidget* parent)
    : QWidget{parent}, ui{new Ui::ProtocolForm()} {
  ui->setupUi(this);

  connect(ui->pushButton, &QPushButton::clicked, ui->listWidget,
          &QListWidget::clear);
  connect(ui->pushButton_2, &QPushButton::clicked, this,
          &ProtocolForm::storeLog);
}

ProtocolForm::~ProtocolForm() { delete ui; }

void ProtocolForm::gameStart(const QString& fahrplan, bool simstart) {
  auto item = new QListWidgetItem(ui->listWidget);

  item->setText(QString{"New Timetable: %1 (%2)"}.arg(fahrplan).arg(
      simstart ? "TRUE" : "FALSE"));
  item->setForeground(Qt::red);

  item->setSelected(true);
  ui->listWidget->scrollToBottom();
}

namespace {
class MyQStringFormatter {
  QString s;

 public:
  MyQStringFormatter(const QString& s) : s{s} {}

  template <typename T>
  MyQStringFormatter& arg_if(std::optional<T>& value) {
    if (value) {
      s = s.arg(*value);
    } else {
      s = s.arg("--");
    }

    return *this;
  }

  MyQStringFormatter& arg_if(std::optional<float>& value) {
    if (value) {
      s = s.arg(static_cast<double>(*value));
    } else {
      s = s.arg("--");
    }
    return *this;
  }

  operator const QString&() const { return s; }
};
}  // namespace

void ProtocolForm::stateChange(ZusiWatcher::ZusiState state) {
  ui->listWidget->addItem(
      MyQStringFormatter{"Data: %1 %2m %3m/s (%4m/s)  %5 %6 %7"}
          .arg_if(state.date)
          //.arg_if(state.time)
          .arg_if(state.distance)
          .arg_if(state.speed)
          .arg_if(state.streckengeschwindigkeit)
          .arg_if(state.drehzahl)
          .arg_if(state.zugkraft)
          .arg_if(state.sifa_hupe));
  ui->listWidget->scrollToBottom();
}

void ProtocolForm::storeLog() {
  auto filename = QFileDialog::getSaveFileName(
      this, "Save log to",
      QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) +
          "/zusilauncherlog.txt");
  if (filename.isNull()) {
    return;
  }

  QFile file{filename};
  if (!file.open(QIODevice::WriteOnly)) {
    QMessageBox dialog;
    dialog.setText("Failed to open " + filename);
    dialog.setIcon(QMessageBox::Critical);
    dialog.setDefaultButton(QMessageBox::Ok);
    return;
  }

  for (int i = 0; i < ui->listWidget->count(); ++i) {
    file.write((ui->listWidget->item(i)->text() + "\r\n").toUtf8());
  }
}
