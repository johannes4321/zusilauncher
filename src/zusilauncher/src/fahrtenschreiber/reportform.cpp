#include "reportform.h"
#include "ui_reportform.h"

#include <QtCharts/QChart>
#include <QtCharts/QLineSeries>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QValueAxis>

#include "../zusilib/zugcache.h"
#include "fahrtenschreiber.h"

extern ZugCache *globalZugCache;

namespace Fahrtenschreiber {
ReportForm::ReportForm(QWidget *parent)
    : QWidget(parent), ui(new Ui::ReportForm) {
  ui->setupUi(this);
  connect(this, &ReportForm::dataChanged, this, &ReportForm::render);
}

void ReportForm::render(qlonglong id, Fahrtenschreiber *fahrtenschreiber) {
  auto header = fahrtenschreiber->getReportHeader(id, this);
  auto data = fahrtenschreiber->getSeries(id);

  auto filename = header.fahrplan();
  auto &zug = globalZugCache->getEntry(filename);

  auto starttime = header.starttime();

  setWindowTitle(tr("ZusiLauncher Fahrtenschreiber - %1 %2 %3 (%4)")
                     .arg(zug.getGattung())
                     .arg(zug.getNummer())
                     .arg(zug.getZuglauf())
                     .arg(starttime));

  auto *chart = new QtCharts::QChart();
  chart->addSeries(data->streckenseries());
  chart->addSeries(data->speedseries());
  chart->createDefaultAxes();
  dynamic_cast<QtCharts::QValueAxis *>(chart->axes(Qt::Horizontal).back())
      ->setLabelFormat("%.0f km");
  dynamic_cast<QtCharts::QValueAxis *>(chart->axes(Qt::Vertical).back())
      ->setLabelFormat("%.0f km/h");
  chart->setTitle(zug.getZuglauf());

  ui->chart->setRenderHint(QPainter::Antialiasing);
  ui->chart->setChart(chart);

  ui->zug->setFahrtenschreiber(fahrtenschreiber);
  ui->zug->setZug(&zug);
}

ReportForm::~ReportForm() { delete ui; }

}  // namespace Fahrtenschreiber
