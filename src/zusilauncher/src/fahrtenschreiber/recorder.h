#ifndef RECORDER_H
#define RECORDER_H

#include <QObject>
#include <QtSql>

#include "../watchercontroller.h"

namespace Fahrtenschreiber {
class Recorder : public QObject {
  int currentId = 0;
  QSqlQuery newFahrtQuery{};
  QSqlQuery newDataQuery{};

  Q_OBJECT
 public:
  explicit Recorder(QSqlDatabase *db, QObject *parent = nullptr);

 signals:

 public slots:
  void gotFahrplan(const QString &fahrplan, bool simstart);
  void stateChanged(ZusiWatcher::ZusiState state);
};
}  // namespace Fahrtenschreiber

#endif  // RECORDER_H
