#ifndef FAHRTENSCHREIBER_H
#define FAHRTENSCHREIBER_H

#include <QObject>
#include <QtSql>

#include "protocolform.h"
#include "recorder.h"
#include "reportform.h"

namespace Fahrtenschreiber {

void registerToQml();

class ReportForm;

class ChartData : public QObject {
  QtCharts::QAbstractSeries *m_speedseries;
  QtCharts::QAbstractSeries *m_streckenseries;

  Q_PROPERTY(QtCharts::QAbstractSeries *speedseries READ speedseries)
  Q_PROPERTY(QtCharts::QAbstractSeries *streckenseries READ streckenseries)

  Q_OBJECT

 public:
  ChartData() = default;

  ChartData(QtCharts::QAbstractSeries *speed,
            QtCharts::QAbstractSeries *strecke, QObject *parent = nullptr)
      : QObject{parent}, m_speedseries{speed}, m_streckenseries{strecke} {}

  Q_INVOKABLE QtCharts::QAbstractSeries *speedseries() const {
    return m_speedseries;
  }
  Q_INVOKABLE QtCharts::QAbstractSeries *streckenseries() const {
    return m_streckenseries;
  }
};

class Fahrtenschreiber : public QObject {
  WatcherController watchcontrol;
  QSqlDatabase db;
  Recorder recorder;
  QString host;
  int port;

  Q_OBJECT

 public:
  static QSqlError initDb();

  class ReportHeader {
    QSqlQueryModel *model;

   public:
    explicit ReportHeader(QSqlQueryModel *model) : model{model} {}

    qlonglong id() const {
      return model->record(0).field(0).value().toLongLong();
    }

    QString fahrplan() const {
      return model->record(0).field(1).value().toString();
    }

    QString starttime() const {
      return model->record(0).field(2).value().toString();
    }
  };

  explicit Fahrtenschreiber(const QString &host, int port,
                            QObject *parent = nullptr);

  // TODO: I don't want to know what a LauncherWindow is!
  void start(LauncherWindow *launcher) {
    watchcontrol.start(host, port, launcher);
  }
  void stop() { watchcontrol.stop(); }

  Recorder *getRecorder() { return &recorder; }

  QAbstractTableModel *getQueryModel(const QString &fplfile,
                                     QObject *parent = nullptr);

  ReportHeader getReportHeader(qlonglong id, QObject *parent = nullptr) const;

  QAbstractTableModel *getFullReport(QObject *parent = nullptr) const;

  ReportForm *getReportForm(qlonglong id, QWidget *parent = nullptr);

  ChartData *getSeries(qlonglong id);
 signals:

 public slots:
  void showLog();

 private:
  QSqlQueryModel *getReportData(qlonglong id, QObject *parent = nullptr) const;
};
}  // namespace Fahrtenschreiber

#endif  // FAHRTENSCHREIBER_H
