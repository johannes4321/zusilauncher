#ifndef FAHRTENSCHREIBERWIDGET_H
#define FAHRTENSCHREIBERWIDGET_H

#include <QWidget>
#include "fahrtenschreiber.h"

namespace Fahrtenschreiber {
namespace Ui {
class FahrtenschreiberWidget;
}

class FahrtenschreiberWidget : public QWidget {
  Q_OBJECT

 public:
  explicit FahrtenschreiberWidget(QWidget *parent = nullptr);
  ~FahrtenschreiberWidget();

  void fahrtenschreiber(Fahrtenschreiber *fahrtenschreiber);

 signals:
  void zugSelected(const QString &filename);

 private:
  Ui::FahrtenschreiberWidget *ui;
  Fahrtenschreiber *m_fahrtenschreiber;
};
}  // namespace Fahrtenschreiber

#endif  // FAHRTENSCHREIBERWIDGET_H
