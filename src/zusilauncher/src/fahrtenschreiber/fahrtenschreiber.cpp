#include "fahrtenschreiber.h"
#include "iconprovider.h"
#include "reportform.h"
#include "zugcache.h"

#include <QLineSeries>
#include <QStandardPaths>

extern ZugCache *globalZugCache;

namespace Fahrtenschreiber {

class FullReportModel : public QSqlQueryModel {
  Q_OBJECT

 public:
  FullReportModel(const QSqlDatabase &db, QObject *parent = nullptr)
      : QSqlQueryModel{parent} {
    setQuery(
        "SELECT fahrten.id, starttime, fahrplan, COUNT(*), "
        "ROUND(MAX(distance)/1000, 2) || "
        "' km' FROM fahrten JOIN fahrtdata ON fahrten.id = fahrtid "
        "GROUP BY fahrtdata.fahrtid ORDER BY fahrten.id DESC",
        db);
    setHeaderData(0, Qt::Horizontal, tr("#"));
    setHeaderData(1, Qt::Horizontal, tr("Startzeit"));
    setHeaderData(2, Qt::Horizontal, tr("Zug"));
    setHeaderData(3, Qt::Horizontal, tr("Messpunkte"));
    setHeaderData(4, Qt::Horizontal, tr("Gefahrene Strecke"));
  }

  QVariant data(const QModelIndex &item,
                int role = Qt::DisplayRole) const override {
    static IconProvider iconprovider{};  // TODO inject me

    if (item.column() != 2) {
      auto base = QSqlQueryModel::data(item, role);
      return base;
    }

    auto base = QSqlQueryModel::data(item, Qt::DisplayRole);

    switch (role) {
      case Qt::DisplayRole: {
        auto &zug = globalZugCache->getEntry(base.toString());
        return zug.getGattung() + " " + zug.getNummer() + " (" +
               zug.getZuglauf() + ")";
      }
      case Qt::ToolTipRole:
        return base;
      case Qt::DecorationRole: {
        auto &zug = globalZugCache->getEntry(base.toString());
        return iconprovider(zug.getGattung());
      }
      default:
        return QVariant{};
    }
  }
};

Fahrtenschreiber::Fahrtenschreiber(const QString &host, int port,
                                   QObject *parent)
    : QObject(parent),
      db{QSqlDatabase::database("fahrtenschreiber", true)},
      recorder{&db},
      host{host},
      port{port} {
  auto ops = watchcontrol.getOps();
  connect(ops, &WatcherOps::gotFahrplan, &recorder, &Recorder::gotFahrplan);
  connect(ops, &WatcherOps::stateChanged, &recorder, &Recorder::stateChanged,
          Qt::QueuedConnection);
}

QSqlError Fahrtenschreiber::initDb() {
  auto db = QSqlDatabase::addDatabase("QSQLITE", "fahrtenschreiber");

  // TODO - The location should be injected
  QDir dir{};
  dir.mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
  db.setDatabaseName(
      QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) +
      "/fahrtenschreiber.sqlite");
  std::cout << "Database file: " << db.databaseName().toStdString() << "\n";
  if (!db.open()) {
    std::cout << "Error " << db.lastError().text().toStdString() << '\n';
    return db.lastError();
  }

  auto tables = db.tables();
  if (!tables.contains("fahrten", Qt::CaseInsensitive)) {
    db.exec(
        "CREATE TABLE fahrten (id INTEGER PRIMARY KEY AUTOINCREMENT, fahrplan "
        "VARCHAR(255), starttime char(19) )");
    std::cout << "ERR " << db.lastError().text().toStdString() << "\n";
    db.exec(
        "CREATE TABLE fahrtdata (id INTEGER PRIMARY KEY, fahrtid INT "
        "REFERENCES "
        "fahrten (id) ON DELETE CASCADE, "
        "distance float, speed float, streckengeschwindigkeit float, zugkraft "
        "float, drehzahl float, ingametime datetime, sifa_hupe int)");
    std::cout << db.lastError().text().toStdString() << "\n";
  }

  db.exec(
      "DELETE FROM fahrtdata WHERE distance IS NULL AND speed IS NULL AND "
      "streckengeschwindigkeit IS NULL AND zugkraft  IS NULL AND   drehzahl IS "
      "NULL AND  ingametime IS NULL AND  sifa_hupe IS NULL");
  db.exec("VACUUM");

  return QSqlError{};
}

QAbstractTableModel *Fahrtenschreiber::getQueryModel(const QString &fplfile,
                                                     QObject *parent) {
  auto model = new QSqlQueryModel(parent);

  // TODO - We have to use the relative path here instead of the LIKE query ...
  model->setQuery(
      "SELECT fahrten.id, starttime, COUNT(*), ROUND(MAX(distance)/1000, 2) || "
      "' km' FROM fahrten JOIN fahrtdata ON fahrten.id = fahrtid WHERE '" +
          fplfile + "' LIKE '%' || fahrplan GROUP BY fahrtdata.fahrtid",
      db);
  model->setHeaderData(0, Qt::Horizontal, tr("#"));
  model->setHeaderData(1, Qt::Horizontal, tr("Startzeit"));
  model->setHeaderData(2, Qt::Horizontal, tr("Messpunkte"));
  model->setHeaderData(3, Qt::Horizontal, tr("Gefahrene Strecke"));

  return model;
}

Fahrtenschreiber::ReportHeader Fahrtenschreiber::getReportHeader(
    qlonglong id, QObject *parent) const {
  auto model = new QSqlQueryModel(parent);
  QString id_s;
  id_s.setNum(id);
  model->setQuery("SELECT * FROM fahrten WHERE id = " + id_s, db);
  model->query();
  return ReportHeader{model};
}

QSqlQueryModel *Fahrtenschreiber::getReportData(qlonglong id,
                                                QObject *parent) const {
  auto model = new QSqlQueryModel(parent);
  QString id_s;
  id_s.setNum(id);
  model->setQuery("SELECT * FROM fahrtdata WHERE fahrtid = " + id_s, db);
  return model;
}

ChartData *Fahrtenschreiber::getSeries(qlonglong id) {
  auto data = this->getReportData(id, this);
  auto speedseries = new QtCharts::QLineSeries();
  speedseries->setName(tr("Geschwindigkeit"));
  auto streckenseries = new QtCharts::QLineSeries();
  streckenseries->setName(tr("Streckengeschwindigkeit"));

  data->query();

  while (data->canFetchMore()) {
    data->fetchMore();
  }

  auto count = data->rowCount();
  qreal last_speed = .0, last_streckenspeed = .0;
  for (int i = 0; i < count; ++i) {
    auto row = data->record(i);
    auto distance = row.field(2).value().toReal() / 1000.;
    if (distance == .0) {
      continue;
    }

    auto appendIfValue = [distance](QtCharts::QLineSeries *series, qreal &last,
                                    QSqlField field) {
      qreal value = field.value().toReal();
      if (value != .0) {
        last = value * 3.6;
      }
      series->append(distance, last);
    };
    appendIfValue(speedseries, last_speed, row.field(3));
    appendIfValue(streckenseries, last_streckenspeed, row.field(4));
  }

  return new ChartData{speedseries, streckenseries,
                       const_cast<Fahrtenschreiber *>(this)};
}

QAbstractTableModel *Fahrtenschreiber::getFullReport(QObject *parent) const {
  auto model = new FullReportModel(db, parent);

  return model;
}

ReportForm *Fahrtenschreiber::getReportForm(qlonglong id, QWidget *parent) {
  auto report = new ReportForm(parent);
  report->id(id);
  report->fahrtenschreiber(this);
  return report;
}

void Fahrtenschreiber::showLog() {
  auto ops = watchcontrol.getOps();
  auto form = new ProtocolForm();

  connect(ops, &WatcherOps::gotFahrplan, form, &ProtocolForm::gameStart);
  connect(ops, &WatcherOps::stateChanged, form, &ProtocolForm::stateChange);

  form->show();
}

}  // namespace Fahrtenschreiber

// must be after FullReport declaration and outside the namespace declaration
#include "fahrtenschreiber.moc"
