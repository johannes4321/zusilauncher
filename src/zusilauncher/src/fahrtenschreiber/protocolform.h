#ifndef PROTOCOLFORM_H
#define PROTOCOLFORM_H

#include <QWidget>

#include <memory>

// Needed for ZusiState, which is passed by-value ...
#include "zusiwatcher.h"

namespace Ui {
class ProtocolForm;
}

class ProtocolForm : public QWidget {
  Q_OBJECT

 public:
  explicit ProtocolForm(QWidget *parent = nullptr);
  ~ProtocolForm();

 public slots:
  void gameStart(const QString &fahrplan, bool simstart);
  void stateChange(ZusiWatcher::ZusiState state);

 private slots:
  void storeLog();

 private:
  Ui::ProtocolForm *ui;
};

#endif  // PROTOCOLFORM_H
