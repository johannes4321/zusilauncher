#ifndef FAHRPLANLISTMODEL_H
#define FAHRPLANLISTMODEL_H

#include <QAbstractItemModel>
#include <QObject>
#include <QString>
#include "fahrplantree.h"
#include "zugcache.h"
#include "zusifahrplan.h"

class QProgressDialog;

class FahrplanItemModel : public QAbstractItemModel {
  Q_OBJECT

 public:
  enum Roles { BegruessungRole = Qt::UserRole, ZuegeRole = Qt::UserRole + 1 };

  FahrplanItemModel(QObject *parent = nullptr);
  ~FahrplanItemModel();

  void setRootPath(const QString &rootPath);
  const QString &rootPath() const;

  void setZugCache(ZugCache *zugCache);
  const ZugCache *zugCache() const;

  QModelIndex index(int row, int column, const QModelIndex &parent) const;
  QModelIndex parent(const QModelIndex &child) const;
  int columnCount(const QModelIndex &parent) const;

  int rowCount(const QModelIndex &parent = QModelIndex()) const;
  QVariant data(const QModelIndex &index, int role) const;
  QVariant headerData(int section, Qt::Orientation orientation,
                      int role = Qt::DisplayRole) const;

  bool hasChildren(const QModelIndex &parent) const;

  FahrplanTreeItemRoot *getRootItem() { return rootItem; }

  void loadAllZuege(bool showProgress = true);
  QString analyze();

 private:
  bool allLoaded;
  QString rootPath_{};
  ZugCache *zugCache_;
  FahrplanTreeItemRoot *rootItem;

  void loadZuege(FahrplanTreeItem *current, int level,
                 QProgressDialog *progress);

 signals:
  void zugSummariesLoaded(const QString &filename, double duration);
};

#endif  // FAHRPLANLISTMODEL_H
