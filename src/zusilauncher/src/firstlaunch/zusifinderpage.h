#ifndef ZUSIFINDERPAGE_H
#define ZUSIFINDERPAGE_H

#include <QWizardPage>

namespace Ui {
class ZusiFinderPage;
}

class ZusiFinderPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit ZusiFinderPage(QWidget *parent = 0);
    ~ZusiFinderPage();

    bool isComplete() const override;

protected:
    void initializePage() override;

private:
    Ui::ZusiFinderPage *ui;

private slots:
    void pickDirectory();
};

#endif // ZUSIFINDERPAGE_H
