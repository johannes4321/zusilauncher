#include "firstlaunchwizard.h"
#include <QCoreApplication>
#include <QSettings>
#include "tcppage.h"
#include "welcomepage.h"
#include "zusifinderpage.h"

#include <iostream>

FirstLaunchWizard::FirstLaunchWizard(QWidget *parent) : QWizard(parent) {
  addPage(new WelcomePage);
  addPage(new ZusiFinderPage);
  addPage(new TcpPage);

#ifndef Q_OS_MAC
  setWizardStyle(ModernStyle);
#endif

  setWindowTitle(tr("ZusiLauncher Setup"));

  QSettings reg(R"(HKEY_LOCAL_MACHINE\Software\WOW6432Node\Zusi3)",
                QSettings::NativeFormat);
  QString zusiDataPath(reg.value("DatenVerzeichnisOffiziell").toString());

  setField("zusiDataPath", zusiDataPath);

  QSettings zusireg(R"(HKEY_CURRENT_USER\Software\Zusi3\Fahrsim\Einstellungen)",
                    QSettings::NativeFormat);

  setField("zusiPort", 1436);
  setField("zusiHost", "127.0.0.1");
  setField("zusiFahrtenschreiber", true);
}

void FirstLaunchWizard::accept() {
  QSettings settings;
  settings.setValue("config/version", 1);

  QString zusiDataPath = field("zusiDataPath").toString();
  if (zusiDataPath[zusiDataPath.length() - 1] != '\\') {
    // Using forward slash for Linux compatibility in testing ...
    zusiDataPath += '/';
  }
  settings.setValue("config/zusiDataPath", zusiDataPath);
  if (field("zusiFahrtenschreiber").toBool()) {
    settings.setValue("config/zusiFahrtenschreiber", true);
    settings.setValue("config/zusiTcpPort", field("zusiPort"));
    settings.setValue("config/zusiTcpHost", field("zusiHost"));

    // Not sure if this is still needed, Zusi seems to always enable the TCP
    // server
    QSettings zusireg(
        R"(HKEY_CURRENT_USER\Software\Zusi3\Fahrsim\Einstellungen)",
        QSettings::NativeFormat);
    zusireg.value("NetzwerkServerAutom").setValue(1);
    zusireg.sync();
  } else {
    settings.setValue("config/zusiFahrtenschreiber", false);
  }

  settings.sync();
  QDialog::accept();
}
