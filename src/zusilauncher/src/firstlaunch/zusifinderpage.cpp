#include "zusifinderpage.h"
#include "ui_zusifinderpage.h"

#include <QDir>
#include <QFileDialog>
#include <QSettings>

ZusiFinderPage::ZusiFinderPage(QWidget *parent)
    : QWizardPage(parent), ui(new Ui::ZusiFinderPage) {
  ui->setupUi(this);

  registerField("zusiDataPath", ui->lineEdit);
}

void ZusiFinderPage::initializePage() {
  ui->lineEdit->setText(field("zusiDataPath").toString());

  connect(ui->pushButton, &QAbstractButton::clicked, this,
          &ZusiFinderPage::pickDirectory);
  connect(ui->lineEdit, &QLineEdit::textChanged, this,
          &QWizardPage::completeChanged);
}

bool ZusiFinderPage::isComplete() const {
  QDir d(ui->lineEdit->text());
  return d.exists() && d.cd("Timetables") && d.cd("..") && d.cd("RollingStock");
}

void ZusiFinderPage::pickDirectory() {
  QString dir = QFileDialog::getExistingDirectory(
      nullptr, tr("Open Directory"), ui->lineEdit->text(),
      QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

  if (dir != "") ui->lineEdit->setText(dir);
}

ZusiFinderPage::~ZusiFinderPage() { delete ui; }
