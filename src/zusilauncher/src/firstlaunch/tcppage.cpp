#include "tcppage.h"
#include "ui_tcppage.h"

TcpPage::TcpPage(QWidget *parent) : QWizardPage(parent), ui(new Ui::TcpPage) {
  ui->setupUi(this);

  registerField("zusiFahrtenschreiber", ui->groupBox, "checked", "toggled");
  registerField("zusiPort", ui->portSpinBox);
  registerField("zusiHost", ui->hostnameLineEdit);
}

TcpPage::~TcpPage() { delete ui; }

void TcpPage::initializePage() {
  ui->groupBox->setChecked(field("zusiFahrtenschreiber").toBool());
}
