#ifndef TCPPAGE_H
#define TCPPAGE_H

#include <QWizardPage>

namespace Ui {
class TcpPage;
}

class TcpPage : public QWizardPage {
  Q_OBJECT

 public:
  explicit TcpPage(QWidget *parent = nullptr);
  ~TcpPage() override;

 protected:
  void initializePage() override;

 private:
  Ui::TcpPage *ui;
};

#endif  // TCPPAGE_H
