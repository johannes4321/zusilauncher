#ifndef LAUNCHERWINDOW_H
#define LAUNCHERWINDOW_H

#include <memory>

#include <QAbstractItemModel>
#include <QLabel>
#include <QMainWindow>
#include <QWebEngineView>
//#include <QtWebKit/QtWebKit>
#include "fahrplanitemmodel.h"
#include "fahrtenschreiber/fahrtenschreiber.h"

class TrainRating;

namespace Ui {
class LauncherWindow;
}

class LauncherWindow : public QMainWindow {
  Q_OBJECT

 public:
  explicit LauncherWindow(QWidget *parent = nullptr);
  void setFahrtenschreiber(
      Fahrtenschreiber::Fahrtenschreiber *fahrtenschreiber);
  void setRootPath(const QString &rootPath);
  void setModel(FahrplanItemModel *model);
  void showInitDuration(double duration);
  ~LauncherWindow();

 public slots:
  void pickFahrplan(const QModelIndex &indexes, const QModelIndex &previous);

  void updateStatus(const QString &status);
  void zusiConnected(const QString &zusiVersion);
  void zusiDisconnected();

 private slots:
  void analyze();

  void zugSelectedFromSearch(const FahrplanTreeItemZug *zug);

 private:
  FahrplanItemModel *model{};
  Fahrtenschreiber::Fahrtenschreiber *fahrtenschreiber{nullptr};
  std::unique_ptr<Ui::LauncherWindow> ui;
  void renderTrainPage(const FahrplanTreeItemZug *item);
  void renderPlanPage(const FahrplanTreeItemPlan *item);
  QWebEngineView *web{nullptr};
  QString rootPath{""};
  QLabel *zusiVersionLabel{nullptr};
};

#endif  // LAUNCHERWINDOW_H
