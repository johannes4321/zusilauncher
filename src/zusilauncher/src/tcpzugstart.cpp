#include "tcpzugstart.h"
#include "zug.h"

#include "Zusi3TCP.h"

#include <boost/asio.hpp>
#include <iostream>

#include <QSettings>

void launchZugviaTCP(const QString &host, int port, const Zug &zug, const QString &fahrplanFile) {
  QSettings settings;
  const QString rootPath{settings.value("config/zusiDataPath").toString()};

  QString filename = fahrplanFile;
   filename
          .replace(rootPath, "")
          .replace('/', '\\');

  try {
    using boost::asio::ip::tcp;
    boost::asio::io_service io_service;
    tcp::resolver::query query{host.toStdString(), std::to_string(port)};
    tcp::socket socket{io_service};

    tcp::resolver resolver(io_service);
    tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
    boost::asio::connect(socket, endpoint_iterator);

    zusi::Connection con(socket, "zusilauncher train start");
    con.connect({}, {}, true);

    con.control(zusi::Control::ZugStart::Fahrplan{filename.toStdString()}, zusi::Control::ZugStart::Zugnummer{zug.getNummer().toStdString()});

  } catch (boost::system::system_error &err) {
    std::cerr << "Boost error: " << err.what() << " (" << err.code() << ')'
              << std::endl;
  } catch (std::domain_error &err) {
    std::cerr << "Domain Exception: " << err.what() << std::endl;
  } catch (std::runtime_error &err) {
    std::cerr << "Runtime Exception: " << err.what() << std::endl;
  }
}
