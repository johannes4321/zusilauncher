#ifndef WATCHERCONTROLLER_H
#define WATCHERCONTROLLER_H

#include <QThread>
#include <iostream>
#include "zusiwatcher.h"

class LauncherWindow;

Q_DECLARE_METATYPE(ZusiWatcher::ZusiState)

class WatcherOps : public QObject, public ZusiWatcher::IOps {
  Q_OBJECT

 public:
  WatcherOps() {}

  // std::shared_ptr<zusi::Socket> getSocket() override;

  void status(const std::string &status) override {
    emit newStatus(QString::fromStdString(status));
  }

  void gameStart(const std::string &fahrplan, bool simstart) override {
    emit gotFahrplan(QString{fahrplan.c_str()}.replace("\\", "/"), simstart);
  }
  void stateChange(ZusiWatcher::ZusiState state) override {
    // TODO: Reduce copies - we need one copy as this is moved between threads
    // and updated in the watcher thread
    emit stateChanged(state);
  }

  void connect(std::string_view version) override {
    emit connected(
        QString::fromUtf8(version.data(), static_cast<int>(version.length())));
  }

  void disconnect() override { emit disconnected(); }

 public slots:
  void doWork(const QString &host, int port) {
    std::cout << "Worker started!" << std::endl;
    ZusiWatcher::watcherThread(host.toStdString(), port, *this);
  }

 signals:
  void newStatus(const QString &status);
  void gotFahrplan(const QString &fahrplan, bool simstart);
  void stateChanged(ZusiWatcher::ZusiState state);
  void connected(const QString &zusiVersion);
  void disconnected();
};

class WatcherController : public QObject {
  Q_OBJECT

  QThread watchThread;
  WatcherOps sig;
 signals:
  void startWatcher(const QString &host, int port);

 public:
  void start(const QString &host, int port, LauncherWindow *launcher);

  void stop() { watchThread.exit(); }

  WatcherOps *getOps() { return &sig; }
};

#endif
