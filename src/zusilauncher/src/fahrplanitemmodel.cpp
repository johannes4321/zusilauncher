#include "fahrplanitemmodel.h"

#include <numeric>

#include <QApplication>
#include <QIcon>
#include <QProgressDialog>

FahrplanItemModel::FahrplanItemModel(

    QObject *parent)
    : QAbstractItemModel(parent),
      allLoaded(false),
      rootItem(new FahrplanTreeItemRoot()) {}

FahrplanItemModel::~FahrplanItemModel() { delete rootItem; }

void FahrplanItemModel::setRootPath(const QString &rootPath) {
  rootPath_ = rootPath;
}

const QString &FahrplanItemModel::rootPath() const { return rootPath_; }

void FahrplanItemModel::setZugCache(ZugCache *zugCache) {
  zugCache_ = zugCache;
}

const ZugCache *FahrplanItemModel::zugCache() const { return zugCache_; }

QModelIndex FahrplanItemModel::index(int row, int column,
                                     const QModelIndex &parent) const {
  if (!hasIndex(row, column, parent)) return QModelIndex();

  FahrplanTreeItem *parentItem;

  if (!parent.isValid())
    parentItem = rootItem;
  else
    parentItem = static_cast<FahrplanTreeItem *>(parent.internalPointer());

  FahrplanTreeItem *childItem = parentItem->getChild(row);
  if (childItem)
    return createIndex(row, column, childItem);
  else
    return QModelIndex();
}

QModelIndex FahrplanItemModel::parent(const QModelIndex &index) const {
  if (!index.isValid()) return QModelIndex();

  FahrplanTreeItem *childItem =
      static_cast<FahrplanTreeItem *>(index.internalPointer());
  FahrplanTreeItem *parentItem = childItem->getParentItem();

  if (parentItem == rootItem) return QModelIndex();

  return createIndex(parentItem->getRow(), 0, parentItem);
}

int FahrplanItemModel::columnCount(const QModelIndex &parent) const {
  if (parent.isValid())
    return 2;  // static_cast<FahrplanTreeItem*>(parent.internalPointer())->columnCount();
  else
    return 2;  // rootItem->columnCount();
}

bool FahrplanItemModel::hasChildren(const QModelIndex &parent) const {
  if (parent.column() > 0) return false;

  if (!parent.isValid()) return true;

  return static_cast<FahrplanTreeItem *>(parent.internalPointer())->getType() !=
         FahrplanTreeItem::TRAIN;
}

int FahrplanItemModel::rowCount(const QModelIndex &parent) const {
  FahrplanTreeItem *parentItem;
  if (parent.column() > 0) return 0;

  if (!parent.isValid())
    parentItem = rootItem;
  else
    parentItem = static_cast<FahrplanTreeItem *>(parent.internalPointer());

  if (parentItem->getType() == FahrplanTreeItem::PLAN) {
    auto plan = dynamic_cast<FahrplanTreeItemPlan *>(parentItem);
    plan->load();
  }

  return parentItem->childCount();
}

QVariant FahrplanItemModel::data(const QModelIndex &index, int role) const {
  if (!index.isValid()) return QVariant();

  FahrplanTreeItem *item =
      static_cast<FahrplanTreeItem *>(index.internalPointer());

  return item->getData(index.column(), role);
}

QVariant FahrplanItemModel::headerData(int section, Qt::Orientation orientation,
                                       int role) const {
  if (role != Qt::DisplayRole) return QVariant();

  if (orientation == Qt::Horizontal) {
    if (section == 1) return tr("Zuglauf");
  }

  return QVariant();
}

void FahrplanItemModel::loadAllZuege(bool showProgress) {
  if (!allLoaded) {
    int progresscount = std::accumulate(
        rootItem->getChildren().begin(), rootItem->getChildren().end(),
        1 + rootItem->childCount(),
        [](int i, FahrplanTreeItem *item) { return i + item->childCount(); });

    QProgressDialog progress(tr("Lade Züge ... "), tr("Abbrechen (zwecklos)"),
                             0, progresscount);
    progress.setWindowTitle(tr("Lade Züge"));
    progress.setCancelButton(nullptr);
    progress.setWindowModality(Qt::WindowModal);
    if (showProgress) {
      progress.show();
      qApp->processEvents();
    }

    loadZuege(rootItem, 0, showProgress ? &progress : nullptr);

    progress.setValue(2);
  }
  allLoaded = true;
}

QString FahrplanItemModel::analyze() {
  int deko = 0;
  QStringList noFahrzeug, invalidEinsatzAb;

  for (auto &it : *zugCache_) {
    auto &zugfile = it.first;
    auto &zug = zugCache_->getEntry(zugfile);
    if (zug.isDeko()) {
      ++deko;
    } else {
      auto frzge = zug.getFahrzeuge();
      if (!frzge.length()) {
        noFahrzeug.append(zug.getGattung() + " " + zug.getNummer());
        continue;
      }

      if (!frzge.first().EinsatzAb.isValid()) {
        invalidEinsatzAb.append(zug.getGattung() + " " + zug.getNummer());
        continue;
      }
    }
  }

  QString result = tr("Anzahl der Züge: %1\nDavon Deko: %2\n\nZüge ohne "
                      "Fahrzeuge (ohne Deko): %3\n%4\nZüge mit "
                      "Führungsfahrzeug ohne EinsatzAb (ohne Deko): %5\n%6")
                       .arg(zugCache_->size())
                       .arg(deko)
                       .arg(noFahrzeug.length())
                       .arg(noFahrzeug.join(", "))
                       .arg(invalidEinsatzAb.length())
                       .arg(invalidEinsatzAb.join(", "));

  return result;
}

void FahrplanItemModel::loadZuege(FahrplanTreeItem *current, int level,
                                  QProgressDialog *progress) {
  for (auto &&child : current->getChildren()) {
    if (level < 2 && progress) {
      // Level 1 has "_Docu" and "Deutschland"
      progress->setValue(progress->value() + 1);
      progress->setLabelText(child->getData(0, Qt::DisplayRole).toString());
      qApp->processEvents();
    }
    loadZuege(child, level + 1, progress);
  }

  if (current->getType() == FahrplanTreeItem::PLAN) {
    auto plan = static_cast<FahrplanTreeItemPlan *>(current);
    plan->load();
  }
}
