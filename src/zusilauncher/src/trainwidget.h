#ifndef TRAINWIDGET_H
#define TRAINWIDGET_H

#include "fahrplanitemmodel.h"
#include "trainrating.h"

class TrainRating;

namespace Fahrtenschreiber {
class Fahrtenschreiber;
}

namespace Ui {
class TrainWidget;
}

class TrainWidget : public QWidget {
  Q_OBJECT

 public:
  explicit TrainWidget(QWidget *parent = nullptr);
  ~TrainWidget();

  void setFahrtenschreiber(
      Fahrtenschreiber::Fahrtenschreiber *fahrtenschreiber);

  void setZug(const Zug *zug);
  void setZug(const FahrplanTreeItemZug *zugItem);

  const Zug &getZug() const { return zug; }

 signals:
  void launchZusi(const Zug &zug) const;
  void launchZusiTimetable(const Zug &zug) const;
  void launchZusiViaTCP(const Zug &zug) const;

 private:
  Ui::TrainWidget *ui;
  Zug zug;
  TrainRating rating;
  Fahrtenschreiber::Fahrtenschreiber *fahrtenschreiber;  // TODO: Instead of
                                                         // this hard dependency
                                                         // the model should
                                                         // come via a
                                                         // signal/slot
};

#endif  // TRAINWIDGET_H
