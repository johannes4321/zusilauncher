#include "trainwidget.h"

#include <QClipboard>
#include <QDesktopServices>
#include <QFileIconProvider>
#include <QMessageBox>
#include <QStringBuilder>

#include "fahrtenschreiber/fahrtenschreiber.h"
#include "fahrtenschreiber/reportform.h"
#include "fahrzeugcache.h"
#include "iconprovider.h"
#include "launcherror/launcherrorform.h"
#include "tcpzugstart.h"
#include "trainrating.h"
#include "ui_trainwidget.h"

extern FahrzeugCache *globalFahrzeugCache;

TrainWidget::TrainWidget(QWidget *parent)
    : QWidget(parent),
      ui(new Ui::TrainWidget),
      rating{},
      fahrtenschreiber{nullptr} {
  ui->setupUi(this);

  QSettings settings;

  connect(ui->pushButton, &QPushButton::clicked,
          [=](bool) { emit launchZusi(zug); });

  connect(ui->launchTimetableButton, &QPushButton::clicked,
          [=](bool) { emit launchZusiTimetable(zug); });

  if (settings.value("config/zusiFahrtenschreiber").toBool()) {
    connect(ui->tcpStartButton, &QPushButton::clicked,
            [=](bool) { emit launchZusiViaTCP(zug); });
  } else {
    ui->tcpStartButton->hide();
  }

  auto fahrtenschreiber = &this->fahrtenschreiber;
  connect(ui->fahrtenschreiberTableView, &QAbstractItemView::doubleClicked,
          [fahrtenschreiber, this](const QModelIndex &idx) {
            auto model = this->ui->fahrtenschreiberTableView->model();
            auto data = model->sibling(idx.row(), 0, idx).data();
            auto id = data.toLongLong();
            (*fahrtenschreiber)->getReportForm(id)->show();
          });

  connect(this, &TrainWidget::launchZusi, [](const Zug &zug) {
    auto url = QUrl::fromLocalFile(zug.getFilename());
    if (!QFileInfo{url.toLocalFile()}.exists()) {
      // TODO: This is extremely similar to the case below ... should be
      // abstracted
      QMessageBox msg;
      msg.setIcon(QMessageBox::Critical);
      msg.setText(
          "Zugdatei nicht gefunden. Womöglich wurde der Zug mit einem Update "
          "gelöscht oder umbenannt.");
      msg.setStandardButtons(QMessageBox::Ok);
      msg.setDetailedText(zug.getGattung() + " " + zug.getNummer() + "\n" +
                          zug.getFilename());
      msg.exec();
      return;
    }
    if (!QDesktopServices::openUrl(url)) {
      auto e = new LaunchErrorForm(zug.getFilename(),
                                   zug.getGattung() + " " + zug.getNummer());
      e->show();
    }
  });

  connect(this, &TrainWidget::launchZusiTimetable, [=](const Zug &zug) {
    // TODO Either clean this (too much magic knowledge) or remove
    // This is only meant as work-around for Zusi not being able to run a .trn
    // file directly
    QString plan = this->ui->fahrplan->text();
    if (plan == "Strecke\\plan.fpl") {
      auto &&zugfile = zug.getFilename();
      plan = zugfile.left(zugfile.lastIndexOf('/')) + ".fpn";
    } else {
      QSettings settings;
      QString rootPath{settings.value("config/zusiDataPath").toString()};
      plan = rootPath + "/Timetables/" + plan;
    }

    QUrl url = QUrl::fromLocalFile(plan);

    if (!QFileInfo{url.toLocalFile()}.exists()) {
      QMessageBox msg;
      msg.setIcon(QMessageBox::Critical);
      msg.setText(
          "Fahrplandatei nicht gefunden. Womöglich wurde der Fahrplan mit "
          "einem Update "
          "gelöscht oder umbenannt.");
      msg.setStandardButtons(QMessageBox::Ok);
      msg.setDetailedText(plan);
      msg.exec();
    }

    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(zug.getGattung() + " " + zug.getNummer());
    if (!QDesktopServices::openUrl(url)) {
      auto e = new LaunchErrorForm(plan, plan);
      e->show();
    }
  });

  auto ui = this->ui;
  connect(this, &TrainWidget::launchZusiViaTCP, [ui](const Zug &zug) {
    QSettings settings;
    const QString &host = settings.value("config/zusiTcpHost").toString();
    int port = settings.value("config/zusiTcpPort").toInt();

    // Duplicated code from above!
    // note that we have the full path here and are ging to remove the root again ...
    QString plan = ui->fahrplan->text();
    if (plan == "Strecke\\plan.fpl") {
      auto &&zugfile = zug.getFilename();
      plan = zugfile.left(zugfile.lastIndexOf('/')) + ".fpn";
    } else {
      QSettings settings;
      QString rootPath{settings.value("config/zusiDataPath").toString()};
      plan = rootPath + "/Timetables/" + plan;
    }

    launchZugviaTCP(host, port, zug, plan);
  });

  connect(ui->ratingComboBox,
          static_cast<void (QComboBox::*)(const QString &)>(
              &QComboBox::currentIndexChanged),
          [this](const QString &text) { this->rating.setRating(zug, text); });
}

TrainWidget::~TrainWidget() { delete ui; }

void TrainWidget::setFahrtenschreiber(
    Fahrtenschreiber::Fahrtenschreiber *fahrtenschreiber) {
  this->fahrtenschreiber = fahrtenschreiber;
  if (!fahrtenschreiber) {
    ui->fahrtenschreiberTableView->hide();
  }
}

void TrainWidget::setZug(const Zug *zug) {
  // TODO: Should probably be injected ...
  static IconProvider iconprovider{};

  this->zug = *zug;
  QString deko = zug->isDeko() ? " <b>DEKOZUG</b>" : "";

  ui->trainNumber->setText(QString("<html><b>") % zug->getGattung() % "</b> " %
                           zug->getNummer() % deko);
  ui->zuglauf->setText(zug->getZuglauf());
  ui->fahrzeit->setText(zug->getStartzeit().toString("hh:mm") % " - " %
                        zug->getEndzeit().toString("hh:mm") % " (" %
                        zug->getFahrzeit().toString("hh:mm") % ")");

  ui->gruppe->setText(zug->getFahrplanGruppe());

  auto icon = iconprovider(zug->getGattung());
  if (icon.isNull()) {
    ui->iconLabel->hide();
  } else {
    auto iconpix = icon.pixmap(64, 32);
    ui->iconLabel->setPixmap(iconpix);
    ui->iconLabel->show();
  }

  QString text{"<html><table>"};
  foreach (auto entry, zug->getFahrplan()) {
    text += QString(
                "<tr><td><b>%1</b></td><td>%2</td><td>%3</td><td><i>%4</i></"
                "td></tr>")
                .arg(entry.Betrst)
                .arg(entry.Ank.toString("hh:mm"))
                .arg(entry.Abf.toString("hh:mm"))
                .arg(entry.FplEintrag);
  }

  text += "</table><br><br><table>";

  for (auto &fzg : zug->getFahrzeuge()) {
    text += "<tr><td><b>" % fzg.BR % " </td><td>" % fzg.Beschreibung %
            "</td><td><i>" % fzg.Farbgebung % "</i></td></tr>";
  }

  if (zug->hasZDA()) {
    text += "<p>" % tr("ZDA Daten") % "</p>";
  }

  text += "</html>";

  ui->textBrowser->setText(text);

  QFileIconProvider prov;
  QFileInfo info(zug->getFilename());
  ui->pushButton->setIcon(prov.icon(info));
  ui->launchTimetableButton->setIcon(prov.icon(info));

  if (fahrtenschreiber) {
    ui->fahrtenschreiberTableView->setModel(
        fahrtenschreiber->getQueryModel(zug->getFilename(), this));
    ui->fahrtenschreiberTableView->show();
  }

  auto element = ui->ratingComboBox;
  auto value = rating.getRating(*zug);

  if (value == Rating::NONE) {
    element->setCurrentIndex(0);
  } else {
    element->setCurrentText(QString{static_cast<char>(value)});
  }
}

void TrainWidget::setZug(const FahrplanTreeItemZug *zugItem) {
  auto &zug = zugItem->getZug();
  setZug(&zug);

  if (zugItem->getParentItem()->getType() == FahrplanTreeItem::GRUPPE) {
    auto *gruppe = dynamic_cast<const FahrplanTreeItemFahrplangruppe *>(
        zugItem->getParentItem());
    ui->gruppe->setText(gruppe->getName());

    if (gruppe->getParentItem()->getType() == FahrplanTreeItem::PLAN) {
      auto *plan =
          dynamic_cast<const FahrplanTreeItemPlan *>(gruppe->getParentItem());
      auto p = plan->getPlan();

      ui->fahrplan->setText(plan->getPlan()->filename);
    } else {
      ui->fahrplan->setText(tr("unbekannter Fahrplan"));
    }
  } else {
    ui->gruppe->setText(tr("unbekannte Gruppe"));
    ui->fahrplan->setText(tr("unbekannter Plan"));
  }
}
