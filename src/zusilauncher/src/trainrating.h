#ifndef TRAINRATING_H
#define TRAINRATING_H

#include <QtSql>

class Zug;

enum class Rating : char {
  NONE = '\0',
  NEGATIVE = 'L',
  NEUTRAL = 'K',
  POSITIVE = 'J'
};

class TrainRating {
  QSqlDatabase db;
  QSqlQuery deleteQuery;
  mutable QSqlQuery lookupQuery;
  QSqlQuery rateQuery;

 public:
  TrainRating();
  Rating getRating(const Zug &zug) const;
  static void openDB();

  void setRating(const Zug &zug, const QString &rating);
  void setRating(const Zug &zug, Rating rating);
  void unsetRating(const Zug &zug) { setRating(zug, Rating::NONE); }
};

#endif  // TRAINRATING_H
