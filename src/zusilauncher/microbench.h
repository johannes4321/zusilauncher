#ifndef MICROBENCH_H
#define MICROBENCH_H

#include <iostream>
#ifdef _WIN32
#include <windows.h>
#else
#include <sys/resource.h>
#include <sys/time.h>
#endif

class MicroBench {
  const char* caption;
  const double start;
  bool running;

 private:
  static double get_time() {
#ifdef _WIN32
    LARGE_INTEGER t, f;
    QueryPerformanceCounter(&t);
    QueryPerformanceFrequency(&f);
    return (double)t.QuadPart / (double)f.QuadPart;
#else
    struct timeval t;
    struct timezone tzp;
    gettimeofday(&t, &tzp);
    return t.tv_sec + t.tv_usec * 1e-6;
#endif
  }

 public:
  MicroBench(const char* caption)
      : caption(caption), start(get_time()), running(true) {}
  double done() {
    double duration = get_time() - start;
    std::cout << caption << ": " << duration << "\n";
    running = false;
    return duration;
  }

  ~MicroBench() {
    if (running) {
      done();
    }
  }
};

#endif  // MICROBENCH_H
