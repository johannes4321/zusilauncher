#ifndef ZUSIWATCHER_H
#define ZUSIWATCHER_H

#include <chrono>
#include <memory>
#include <optional>
#include <string>

/**
 * The purpose of this library is to enforce a clear boundray of Qt types.
 * The implementation uses no Qt types.
 */

namespace ZusiWatcher {

struct ZusiState {
  std::optional<float> distance;
  std::optional<float> speed;
  std::optional<float> streckengeschwindigkeit;
  std::optional<float> zugkraft;
  std::optional<float> drehzahl;
  struct time_t {
    std::chrono::hours h;
    std::chrono::minutes m;
    std::chrono::seconds s;
  };
  std::optional<time_t> time;
  std::optional<float> date;
  std::optional<bool> sifa_hupe;
};

struct IOps {
  virtual ~IOps() = default;
  // virtual std::shared_ptr<zusi::Socket> getSocket() = 0;
  virtual void status(const std::string &status) = 0;
  virtual void gameStart(const std::string &fahrplan, bool simstart) = 0;
  virtual void stateChange(const ZusiState state) = 0;
  virtual void connect(std::string_view version) = 0;
  virtual void disconnect() = 0;
};

[[noreturn]] void watcherThread(const std::string &host, int port,
                                IOps &messenger);
}  // namespace ZusiWatcher

#endif  // ZUSIWATCHER_H
