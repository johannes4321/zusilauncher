#include "zusiwatcher.h"
#include "Zusi3TCP.h"

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <thread>

using boost::asio::ip::tcp;

namespace ZusiWatcher {
std::ostream &operator<<(std::ostream &os, const ZusiState &state) {
  os << state.time->h.count() << ':' << state.time->m.count() << ':'
     << state.time->s.count() << " " << roundf(*state.speed * 3.6f) << "km/h / "
     << roundf(*state.distance) << "m"
     << (state.sifa_hupe && *state.sifa_hupe ? " SIFA" : "");
  return os;
}

template <typename SourceType, typename TargetType>
void assignToState(const zusi::FtdDataMessage &msg, TargetType &field,
                   bool &have_value) {
  auto val = msg.get<SourceType>();
  if (val) {
    field = TargetType{*val};
    have_value = true;
  }
}

namespace {
template <class... Ts>
struct overloaded : Ts... {
  using Ts::operator()...;
};
template <class... Ts>
overloaded(Ts...)->overloaded<Ts...>;
}  // namespace

static void parseDataMessage(IOps &ops, const zusi::MessageVariant &msg) {
  std::visit(
      overloaded{[&](const zusi::FtdDataMessage &ftdmsg) {
                   ZusiState state{};
                   bool have_value{false};

                   assignToState<zusi::FS::Gesamtweg>(ftdmsg, state.distance,
                                                      have_value);
                   assignToState<zusi::FS::Geschwindigkeit>(ftdmsg, state.speed,
                                                            have_value);
                   assignToState<zusi::FS::Streckenhoechstgeschwindigkeit>(
                       ftdmsg, state.streckengeschwindigkeit, have_value);
                   assignToState<zusi::FS::ZugkraftGesamt>(
                       ftdmsg, state.zugkraft, have_value);
                   assignToState<zusi::FS::Motordrehzahl>(
                       ftdmsg, state.drehzahl, have_value);
                   assignToState<zusi::FS::Datum>(ftdmsg, state.date,
                                                  have_value);
                   assignToState<zusi::FS::UhrzeitStunde>(ftdmsg, state.time->h,
                                                          have_value);
                   assignToState<zusi::FS::UhrzeitMinute>(ftdmsg, state.time->m,
                                                          have_value);
                   assignToState<zusi::FS::UhrzeitSekunde>(
                       ftdmsg, state.time->s, have_value);

                   auto sifa = ftdmsg.get<zusi::FS::Sifa>();
                   if (sifa) {
                     auto hupe = sifa->get<zusi::Sifa::Hupe>();
                     if (hupe) {
                       state.sifa_hupe = *hupe;
                     }
                     have_value = true;
                   }

                   if (have_value) {
                     std::stringstream ss;
                     ss << state;
                     ops.status(ss.str());
                     ops.stateChange(state);
                   }
                 },
                 [](const zusi::OperationDataMessage & /*opmsg*/) {},
                 [&](const zusi::ProgDataMessage &progmsg) {
                   auto simstart = progmsg.get<zusi::ProgData::SimStart>();
                   auto zugdatei = progmsg.get<zusi::ProgData::Zugdatei>();
                   // if (simstart && zugdatei) {
                   ops.gameStart(**zugdatei, !!simstart);
                   //}
                 }

      },
      msg);
}

[[noreturn]] void watcherThread(const std::string &host, int port, IOps &ops) {
  boost::asio::io_service io_service;
  do {
    try {
      tcp::resolver::query query{host, std::to_string(port)};
      tcp::socket socket{io_service};

      boost::system::error_code error;
      tcp::resolver resolver(io_service);
      tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
      boost::asio::connect(socket, endpoint_iterator, error);

      if (!error) {
        zusi::Connection con(socket, "zusilauncher");
        std::vector<zusi::FuehrerstandData> fd_ids{
            zusi::FS::Gesamtweg::id,
            zusi::FS::Geschwindigkeit::id,
            zusi::FS::Streckenhoechstgeschwindigkeit::id,
            zusi::FS::ZugkraftGesamt::id,
            zusi::FS::Motordrehzahl::id,
            zusi::FS::UhrzeitStunde::id,
            zusi::FS::UhrzeitMinute::id,
            zusi::FS::UhrzeitSekunde::id,
            zusi::FS::Datum::id,
            zusi::FS::Sifa::id};

        std::vector<uint16_t> prog_ids{zusi::ProgData::SimStart::id,
                                       zusi::ProgData::Zugdatei::id};

        // Subscribe to receive status updates about the above variables
        // Not subscribing to input events
        con.connect(fd_ids, prog_ids, false);

        ops.connect(con.getZusiVersion());

        std::cout << "Zusi Version:" << con.getZusiVersion() << std::endl;
        std::cout << "Connection Info: " << con.getConnectionnfo() << std::endl;

        do {
          auto msg{con.receiveMessage()};

          parseDataMessage(ops, std::move(msg));
        } while (true);
      }
    } catch (boost::system::system_error &err) {
      std::cerr << "Boost error: " << err.what() << " (" << err.code() << ')'
                << std::endl;
    } catch (std::domain_error &err) {
      std::cerr << "Domain Exception: " << err.what() << std::endl;
    } catch (std::runtime_error &err) {
      std::cerr << "Runtime Exception: " << err.what() << std::endl;
    }
    ops.disconnect();

    std::this_thread::sleep_for(std::chrono::seconds{2});
  } while (true);
  ops.status("End");
}
}  // namespace ZusiWatcher
