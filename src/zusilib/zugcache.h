#ifndef ZUGCACHE_H
#define ZUGCACHE_H

#include "itemcache.h"
#include "zug.h"
#include "zugparser.h"

using ZugCache = ItemCache<Zug, ZugParser>;

#endif // ZUGCACHE_H
