#include "fahrzeugparser.h"

#include <QFile>

FahrzeugParser::FahrzeugParser()
{
}

void FahrzeugParser::readFahrzeugVariante(Fahrzeug *fzg)
{
    Fahrzeug::Variante variante;
    variante.Beschreibung = xml.attributes().value("Beschreibung").toString();
    variante.BR = xml.attributes().value("BR").toString();
    variante.EinsatzAb = QDate::fromString(xml.attributes().value("EinsatzAb").toString(), Qt::ISODate);
    variante.Farbgebung = xml.attributes().value("Farbgebung").toString();

    QString IDHaupt = xml.attributes().value("IDHaupt").toString(),
            IDNeben = xml.attributes().value("IDNeben").toString();

    while (xml.readNextStartElement()) {
        if (xml.name() == "DateiAussenansicht") {
            variante.DateiAussenansicht = xml.attributes().value("Dateiname").toString().replace("\\", "/");
        } else if (xml.name() == "DateiFuehrerstand") {
            variante.DateiFuehrerstand = xml.attributes().value("Dateiname").toString().replace("\\", "/");
        }
        xml.skipCurrentElement();
    }

    fzg->addVariante(IDHaupt, IDNeben, variante);
}

void FahrzeugParser::readFahrzeug(Fahrzeug *fzg)
{
    while (xml.readNextStartElement()) {
        if (xml.name() == "FahrzeugVariante")
            readFahrzeugVariante(fzg);
        else
            xml.skipCurrentElement();
    }
}

void FahrzeugParser::readZusi(Fahrzeug *fzg)
{
    while (xml.readNextStartElement()) {
        if (xml.name() == "Fahrzeug")
            readFahrzeug(fzg);
        else
            xml.skipCurrentElement();
    }
}

Fahrzeug FahrzeugParser::read(const QString &filename)
{
    QFile file{filename};
    if (!file.open(QFile::ReadOnly | QFile::Text))
        return {};

    Fahrzeug fzg;

    fzg.filename = filename;//.mid(rootPath.length() + sizeof("RollingStock/") - 1);
    xml.setDevice(&file);

    if (xml.readNextStartElement()) {

        if (xml.name() == "Zusi") {
            readZusi(&fzg);
        } else {
            // ERROR
        }
    }


    return fzg;
}
