#include "zugparser.h"

#include <QFile>
#include <QXmlStreamReader>
#include "fahrzeugcache.h"

extern FahrzeugCache *globalFahrzeugCache;

ZugParser::ZugParser()
{

}

Zug ZugParser::read(const QString &filename)
{
    QFile file{filename};
    if (!file.open(QFile::ReadOnly | QFile::Text))
        return {"X", true, "X", "0", Zug::Gueterzug, "FEHLER", "FEHLER", {}, {}};

    QXmlStreamReader xml;
    xml.setDevice(&file);

    bool Deko{false};
    QString Gattung, Nummer, Zuglauf, FahrplanGruppe;
    Zug::Zugtyp Typ = Zug::Gueterzug;
    QVector<Zug::FahrplanEintrag> entries;
    QVector<Fahrzeug::Variante> fahrzeuge;

    xml.readNextStartElement(); // <Zusi>
    while (xml.readNextStartElement()) {
        if (xml.name() == "Zug") {
            Deko = xml.attributes().value("Dekozug").toInt() == 1;
            Gattung = xml.attributes().value("Gattung").toString();
            Nummer = xml.attributes().value("Nummer").toString();
            Typ = (xml.attributes().value("Zugtyp").toString() == "1") ? Zug::Reisezug : Zug::Gueterzug;
            Zuglauf = xml.attributes().value("Zuglauf").toString();
            FahrplanGruppe = xml.attributes().value("FahrplanGruppe").toString();

            while (xml.readNextStartElement()) {
                if (xml.name() == "FahrplanEintrag") {
                    Zug::FahrplanEintrag entry;
                    entry.Abf = QDateTime::fromString(xml.attributes().value("Abf").toString(), "yyyy-MM-dd HH:mm:ss");
                    entry.Ank = QDateTime::fromString(xml.attributes().value("Ank").toString(), "yyyy-MM-dd HH:mm:ss");
                    entry.Betrst = xml.attributes().value("Betrst").toString();
                    entry.FplEintrag = xml.attributes().value("FplEintrag").toString();
                    entry.FzgVerbandAktion = xml.attributes().value("FzgVerbandAktion").toString();
                    entries.append(entry);
                    xml.skipCurrentElement();
                } else if (xml.name() == "FahrzeugVarianten") {
                    while (xml.readNextStartElement()) {
                        if (xml.name() == "FahrzeugInfo") {
                            QString IDHaupt = xml.attributes().value("IDHaupt").toString();
                            QString IDNeben = xml.attributes().value("IDNeben").toString();

                            while (xml.readNextStartElement()) {
                                if (xml.name() == "Datei") {
                                    auto fzg = globalFahrzeugCache->getEntry(xml.attributes().value("Dateiname").toString().replace("\\", "/"));
                                    if (fzg.varianteExists(IDHaupt, IDNeben)) {
                                        fahrzeuge.append(fzg.variante(IDHaupt, IDNeben));
                                    }
                                }
                                xml.readNextStartElement();
                            }
                        } else {
                            xml.skipCurrentElement();
                        }
                    }
                } else {
                    xml.skipCurrentElement();
                }
            }
        }

        xml.skipCurrentElement();
    }


    return {filename, Deko, Gattung, Nummer, Typ, Zuglauf, FahrplanGruppe, entries, fahrzeuge};
}
