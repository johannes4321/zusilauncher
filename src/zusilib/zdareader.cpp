#include "zdareader.h"

#include "zug.h"

ZDAReader::ZDAReader(const QString &filename) : file{filename} {
  if (!file.open(QFile::ReadOnly)) {
    return;
  }

  xml.setDevice(&file);
}
bool ZDAReader::hasZug(const QString &fpn, const Zug &zug) {
  if (xml.readNextStartElement()) {
    if (xml.name() == "Railway") {
      if (readRailway(fpn, zug)) {
        return true;
      }
      xml.skipCurrentElement();
    }
  }
  return false;
}

bool ZDAReader::readRailway(const QString &fpn, const Zug &zug) {
  while (xml.readNextStartElement()) {
    if (xml.name() == "Line") {
      if (readLine(fpn, zug)) {
        return true;
      }
    }
    xml.skipCurrentElement();
  }

  return false;
}

bool ZDAReader::readLine(const QString &fpn, const Zug &zug) {
  while (xml.readNextStartElement()) {
    if (xml.name() == "Track") {
      if (readTrack(fpn, zug)) {
        return true;
      }
    }
    xml.skipCurrentElement();
  }

  return false;
}

bool ZDAReader::readTrack(const QString &fpn, const Zug &zug) {
  while (xml.readNextStartElement()) {
    if (xml.name() == "Trains") {
      if (readTrains(fpn, zug)) {
        return true;
      }
    }
    xml.skipCurrentElement();
  }

  return false;
}

bool ZDAReader::readTrains(const QString &fpn, const Zug &zug) {
  const QStringRef timetable =
      xml.attributes().value(QStringLiteral("Timetable"));
  if (!timetable.endsWith(fpn)) {
    return false;
  }

  if (readTrain(zug)) {
    return true;
  }

  return false;
}

bool ZDAReader::readTrain(const Zug &zug) {
  while (xml.readNextStartElement()) {
    if (xml.name() != "Train") {
      return false;
    }

    auto type = xml.attributes().value(QStringLiteral("Type"));
    auto number = xml.attributes().value(QStringLiteral("Number"));

    if (type == zug.getGattung() && number == zug.getNummer()) {
      return true;
    }
    xml.skipCurrentElement();
  }

  return false;
}
