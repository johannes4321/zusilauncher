#ifndef ZUGPARSER_H
#define ZUGPARSER_H

#include "zug.h"

class ZugParser
{
public:
    ZugParser();
    Zug read(const QString &);
};

#endif // ZUGPARSER_H
