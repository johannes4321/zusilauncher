#include "zusicache.h"

#include <iostream>

#include <QDataStream>
#include <QFile>
#include <QFileInfo>

ZusiCache::ZusiCache(const QString &rootPath, QFile &cacheFile)
    : rootPath{rootPath},
      cacheFile{cacheFile},
      zugCache_{rootPath},
      fahrzeugCache_{rootPath} {
  if (!cacheFile.open(QIODevice::ReadOnly)) {
    return;
  }
  QDataStream in(&cacheFile);
  auto cacheInfo = QFileInfo{cacheFile};
  auto cacheModified = cacheInfo.lastModified();

  QString header;
  quint64 fahrzeug_size, zug_size;
  in >> header >> fahrzeug_size >> zug_size;

  if (header != header || fahrzeug_size != sizeof(Fahrzeug) ||
      zug_size != sizeof(Zug)) {
    cacheFile.close();
    return;
  }

  in >> fahrzeugCache_ >> zugCache_;

  cacheFile.close();
  if (in.status() != QDataStream::Ok) {
    return;
  }

  for (auto [file, zug] : zugCache_) {
    QFileInfo info(rootPath + file);
    if (!info.exists() || cacheModified < info.lastModified()) {
      // delete from cache
    }
  }

  std::cout << "Cache load succeeded\n";
}

void ZusiCache::persist() {
  if (!cacheFile.open(QIODevice::WriteOnly)) {
    return;
  }

  QDataStream out(&cacheFile);
  out << header << static_cast<quint64>(sizeof(Fahrzeug))
      << static_cast<quint64>(sizeof(Zug)) << fahrzeugCache_ << zugCache_;
  cacheFile.close();
}

ZusiCache::~ZusiCache() {}

const QString ZusiCache::header{"zusiLauncherCache"};
