#ifndef ICONPROVIDER_H
#define ICONPROVIDER_H

#include <QIcon>
#include <QMap>
#include <QString>

#include <unordered_map>

class IconProvider {
 private:
  QIcon empty{};
  QMap<QString, QIcon> mapping{};

 public:
  IconProvider(const QString &root = ":/gattungsicons/images/");

  const QIcon &operator()(const QString &gattung) const;
};

#endif  // ICONPROVIDER_H
