#ifndef ZDAREADER_H
#define ZDAREADER_H

#include <QFile>
#include <QXmlStreamReader>

class Zug;

class ZDAReader {
  QFile file;
  QXmlStreamReader xml{};

  bool readTrain(const Zug &zug);
  bool readTrains(const QString &fpn, const Zug &zug);
  bool readTrack(const QString &fpn, const Zug &zug);
  bool readLine(const QString &fpn, const Zug &zug);
  bool readRailway(const QString &fpn, const Zug &zug);

 public:
  ZDAReader(const QString &filename);

  bool hasZug(const QString &fpn, const Zug &zug);
};

#endif  // ZDAREADER_H
