#include "fahrplanparser.h"
#include "zugparser.h"
#include <QFile>
#include <QDateTime>

void FahrplanParser::readZug(ZusiFahrplan &plan)
{
    while (xml.readNextStartElement()) {
        if (xml.name() == "Datei") {
            plan.zugFiles.push_back(xml.attributes().value("Dateiname").toString().replace("\\", "/"));
        }
        xml.skipCurrentElement();
    }
}

void FahrplanParser::readStrModul(ZusiFahrplan &plan)
{
    while (xml.readNextStartElement()) {
        if (xml.name() == "Datei") {
            plan.strModulFiles.push_back(xml.attributes().value("Dateiname").toString().replace("\\", "/"));
        }
        xml.skipCurrentElement();
    }
}

void FahrplanParser::readFahrplan(ZusiFahrplan &plan)
{
    plan.anfangsZeit = QDateTime::fromString(xml.attributes().value("AnfangsZeit").toString(), "yyyy-MM-dd HH:mm:ss");
    while (xml.readNextStartElement()) {
        if (xml.name() == "Begruessungsdatei") {
            plan.begruessungsDatei = xml.attributes().value("Dateiname").toString();
            xml.skipCurrentElement();
         } else if (xml.name() == "Zug") {
            readZug(plan);
        } else if (xml.name() == "StrModul") {
            readStrModul(plan);
        } else {
            xml.skipCurrentElement();
        }
    }
}


void FahrplanParser::readInfo(ZusiFahrplan &)
{
    while (xml.readNextStartElement()) {
        xml.skipCurrentElement();
    }
}

void FahrplanParser::readZusi(ZusiFahrplan &plan)
{
    while (xml.readNextStartElement()) {
        if (xml.name() == "Info")
            readInfo(plan);
        else if (xml.name() == "Fahrplan")
            readFahrplan(plan);
        else
            xml.skipCurrentElement();
    }
}

QSharedPointer<ZusiFahrplan> FahrplanParser::read(const QString &filename)
{
    QFile file{filename};
    if (!file.open(QFile::ReadOnly | QFile::Text))
        return {};


    QSharedPointer<ZusiFahrplan> plan{new ZusiFahrplan} ;
    plan->filename = filename.mid(rootPath.length() + sizeof("Timetables/") - 1);
    xml.setDevice(&file);

    if (xml.readNextStartElement()) {

        if (xml.name() == "Zusi") {
            readZusi(*plan);
        } else {
            // ERROR
        }
    }

    return plan;
}
