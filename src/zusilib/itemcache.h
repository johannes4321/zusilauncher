#ifndef ITEMCACHE_H
#define ITEMCACHE_H

#include <QDataStream>
#include <QHash>
#include <QString>
#include <unordered_map>

#include <iostream>

// QT_VERSION < 5.14.0
#if QT_VERSION < 0x050E00
namespace std {
template <>
struct hash<QString> {
  using argument_type = QString;
  using result_type = std::size_t;

  result_type operator()(argument_type const& s) const noexcept { return qHash(s); }
};
}  // namespace std
#endif

template <typename ElementType, typename ParserType>
class ItemCache {
 public:
  using CacheT = std::unordered_map<QString, ElementType>;

 private:
  CacheT cache{};
  ParserType parser;
  QString rootPath;

  const ElementType& fillCache(const QString& filename) {
    ElementType element = parser.read(rootPath + filename);
    return cache[filename] = element;
  }

 public:
  using Type = ElementType;
  using size_type = typename CacheT::size_type;
  using iterator = typename CacheT::iterator;
  using const_iterator = typename CacheT::const_iterator;

  explicit ItemCache(const QString& rootPath) : rootPath(rootPath) {}
  ItemCache(const QString& rootPath, const CacheT& cache) : rootPath{rootPath}, cache{cache} {}
  ItemCache(const ItemCache&) = delete;

  void insert(const QString& file, const ElementType& element) {
    cache.emplace(file, std::move(element));
  }

  const ElementType& getEntry(const QString& filename) {
    auto entry = cache.find(filename);
    if (entry != cache.end()) {
      return entry->second;
    }

    return fillCache(filename);
  }

  iterator begin() { return cache.begin(); }
  iterator end() { return cache.end(); }

  const_iterator begin() const { return cache.begin(); }
  const_iterator end() const { return cache.end(); }

  const_iterator cbegin() const { return cache.cbegin(); }
  const_iterator cend() const { return cache.cend(); }

  size_type size() const { return cache.size(); }
};

template <typename ElementType, typename ParserType>
QDataStream& operator<<(QDataStream& os, const ItemCache<ElementType, ParserType>& cache) {
  os << static_cast<quint64>(cache.size());

  for (auto& [file, zug] : cache) {
    os << file << zug;
  }

  return os;
}

template <typename ElementType, typename ParserType>
QDataStream& operator>>(QDataStream& is, ItemCache<ElementType, ParserType>& cache) {
  quint64 count;
  is >> count;

  for (quint64 i = 0; i < count; ++i) {
    QString filename;
    ElementType element;

    is >> filename >> element;
    cache.insert(filename, std::move(element));
  }

  return is;
}

#endif  // ITEMCACHE_H
