#ifndef FAHRZEUGCACHE_H
#define FAHRZEUGCACHE_H

#include "itemcache.h"
#include "fahrzeug.h"
#include "fahrzeugparser.h"

using FahrzeugCache = ItemCache<Fahrzeug, FahrzeugParser>;

#endif // FAHRZEUGCACHE_H
