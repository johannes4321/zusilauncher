#include "zug.h"

#include "zdareader.h"

#include <QDataStream>
#include <QDir>
#include <QFileInfo>

QTime Zug::getFahrzeit() const {
  qint64 duration = getStartzeit().secsTo(getEndzeit()) / 60;
  return {static_cast<int>(duration) / 60, static_cast<int>(duration) % 60};
}

bool parseZDAFile(const QString &filename, const QString &fpn, const Zug &zug) {
  ZDAReader rdr{filename};
  return rdr.hasZug(fpn, zug);
}

bool Zug::hasZDA() const {
  QFileInfo zugfile{filename};
  QDir plandir = zugfile.dir();
  QDir zdadir = plandir;

  if (!zdadir.cdUp() || !zdadir.cd(QStringLiteral("ZusiDisplay"))) {
    return false;
  }

  QString fpn = plandir.dirName() + ".fpn";

  for (auto &zdafilename : zdadir.entryList()) {
    if (!zdafilename.endsWith(QStringLiteral(".zda"))) {
      continue;
    }
    if (parseZDAFile(zdadir.path() + "/" + zdafilename, fpn, *this)) {
      return true;
    }
  }

  return false;
}

QDataStream &operator<<(QDataStream &os, const Zug::FahrplanEintrag &eintrag) {
  os << eintrag.Abf << eintrag.Ank << eintrag.Betrst << eintrag.FplEintrag
     << eintrag.FzgVerbandAktion;

  return os;
}

QDataStream &operator>>(QDataStream &is, Zug::FahrplanEintrag &eintrag) {
  is >> eintrag.Abf >> eintrag.Ank >> eintrag.Betrst >> eintrag.FplEintrag >>
      eintrag.FzgVerbandAktion;

  return is;
}

QDataStream &operator<<(QDataStream &os, const Zug &zug) {
  os << zug.getFilename() << zug.isDeko() << zug.getGattung() << zug.getNummer()
     << (zug.getZugtyp() == Zug::Reisezug) << zug.getZuglauf()
     << zug.getFahrplanGruppe() << zug.getFahrzeuge() << zug.getFahrplan();
  return os;
}
QDataStream &operator>>(QDataStream &is, Zug &zug) {
  bool reisezug;
  is >> zug.filename >> zug.Deko >> zug.Gattung >> zug.Nummer >> reisezug >>
      zug.Zuglauf >> zug.FahrplanGruppe >> zug.fahrzeuge >> zug.fahrplan;
  zug.Typ = reisezug ? Zug::Reisezug : Zug::Gueterzug;
  return is;
}
