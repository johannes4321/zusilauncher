#ifndef FAHRPLANPARSER_H
#define FAHRPLANPARSER_H

#include <QXmlStreamReader>
#include <QSharedPointer>
#include "zusifahrplan.h"
#include "zugcache.h"

class FahrplanParser
{
    QXmlStreamReader xml;
    QString rootPath;
    ZugCache &zugCache;

    void readZug(ZusiFahrplan &plan);
    void readStrModul(ZusiFahrplan &plan);
    void readFahrplan(ZusiFahrplan &plan);
    void readInfo(ZusiFahrplan &plan);
    void readZusi(ZusiFahrplan &plan);

public:
    FahrplanParser(QString rootPath, ZugCache &zugCache) : rootPath(rootPath), zugCache(zugCache) {}
    QSharedPointer<ZusiFahrplan> read(const QString &);
};

#endif // FAHRPLANPARSER_H
