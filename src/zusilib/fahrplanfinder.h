#ifndef FAHRPLANFINDER_H
#define FAHRPLANFINDER_H

#include <QDir>
#include <QStringList>

class FahrplanFinder
{
    QStringList files;
public:
    FahrplanFinder();
    void find(const QDir &root);
    const QStringList &getList() const;
};

#endif // FAHRPLANFINDER_H
