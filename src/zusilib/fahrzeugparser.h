#ifndef FAHRZEUGPARSER_H
#define FAHRZEUGPARSER_H

#include "fahrzeug.h"
#include "QString"
#include "QXmlStreamReader"

class FahrzeugParser
{
public:
    FahrzeugParser();

    Fahrzeug read(const QString &file);
private:
    QXmlStreamReader xml;

    void readZusi(Fahrzeug *fzg);
    void readFahrzeug(Fahrzeug *fzg);
    void readFahrzeugVariante(Fahrzeug *fzg);
};

#endif // FAHRZEUGPARSER_H
