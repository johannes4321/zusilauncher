#ifndef ZUG_H
#define ZUG_H

#include "fahrzeug.h"

#include <QDateTime>
#include <QString>
#include <QVector>

class QDataStream;

class Zug {
  friend QDataStream &operator>>(QDataStream &os, Zug &zug);

 public:
  struct FahrplanEintrag {
    QDateTime Ank, Abf;
    QString Betrst, FplEintrag, FzgVerbandAktion;
  };

  enum Zugtyp { Reisezug, Gueterzug };

  Zug() {}
  Zug(const QString &filename, bool Deko, const QString &Gattung,
      const QString &Nummer, Zugtyp Typ, const QString &Zuglauf,
      const QString &FahrplanGruppe, const QVector<FahrplanEintrag> &fahrplan,
      const QVector<Fahrzeug::Variante> &fahrzeuge)
      : filename(filename),
        Deko(Deko),
        Gattung(Gattung),
        Nummer(Nummer),
        Typ(Typ),
        Zuglauf(Zuglauf),
        FahrplanGruppe(FahrplanGruppe),
        fahrplan(fahrplan),
        fahrzeuge(fahrzeuge) {}
  // Zug(const Zug &) = delete;  // There is no technical need to disallow this,
  //                            // prevents implicit copies

 private:
  QString filename;
  bool Deko;
  QString Gattung;
  QString Nummer;
  Zugtyp Typ;
  QString Zuglauf;
  QString FahrplanGruppe;
  QVector<FahrplanEintrag> fahrplan;
  QVector<Fahrzeug::Variante> fahrzeuge;

 public:
  const QString &getFilename() const { return filename; }

  bool isDeko() const { return Deko; }

  const QString &getGattung() const { return Gattung; }

  const QString &getNummer() const { return Nummer; }

  Zugtyp getZugtyp() const { return Typ; }

  const QString &getZuglauf() const { return Zuglauf; }

  const QString &getFahrplanGruppe() const { return FahrplanGruppe; }

  const QVector<FahrplanEintrag> &getFahrplan() const { return fahrplan; }

  QDateTime getStartzeit() const {
    if (fahrplan.isEmpty()) {
      return {};
    }
    auto first = fahrplan.first();
    return first.Abf.isValid() ? first.Abf : first.Ank;
  }

  QDateTime getEndzeit() const {
    if (fahrplan.isEmpty()) {
      return {};
    }
    auto last = fahrplan.last();
    return last.Ank.isValid() ? last.Ank : last.Abf;
  }

  QTime getFahrzeit() const;

  const QVector<Fahrzeug::Variante> &getFahrzeuge() const { return fahrzeuge; }

  bool hasZDA() const;
};

QDataStream &operator<<(QDataStream &os, const Zug::FahrplanEintrag &eintrag);
QDataStream &operator>>(QDataStream &is, Zug::FahrplanEintrag &eintrag);
QDataStream &operator<<(QDataStream &os, const Zug &zug);
QDataStream &operator>>(QDataStream &is, Zug &zug);
#endif  // ZUG_H
