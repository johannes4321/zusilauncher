#include "fahrplantree.h"

#include "iconprovider.h"

extern ZugCache *globalZugCache;

FahrplanTreeItem::FahrplanTreeItem(Type type, FahrplanTreeItem *parentItem)
    : parentItem(parentItem), type(type) {}

FahrplanTreeItem *FahrplanTreeItem::getChild(int row) {
  return childItems[row];
}

FahrplanTreeItem *FahrplanTreeItem::getChild(int row) const {
  return childItems[row];
}

void FahrplanTreeItem::appendChild(FahrplanTreeItem *child) {
  childItems.append(child);
}

int FahrplanTreeItem::childCount() const { return childItems.length(); }

int FahrplanTreeItem::columnCount() const { return 1; }

int FahrplanTreeItem::getRow() const {
  if (parentItem)
    return parentItem->childItems.indexOf(const_cast<FahrplanTreeItem *>(this));

  return 0;
}

FahrplanTreeItem *FahrplanTreeItem::getParentItem() { return parentItem; }

const FahrplanTreeItem *FahrplanTreeItem::getParentItem() const {
  return parentItem;
}

FahrplanTreeItemRoot::FahrplanTreeItemRoot() : FahrplanTreeItem(ROOT) {}

void FahrplanTreeItemRoot::addFahrplan(QSharedPointer<ZusiFahrplan> plan) {
  auto parts = plan->filename.split("/");
  addPath(plan, parts, 0, this);
}

void FahrplanTreeItemRoot::addPath(QSharedPointer<ZusiFahrplan> plan,
                                   const QStringList &list, int offset,
                                   FahrplanTreeItem *parent) {
  if (offset >= list.length()) return;

  if (offset + 1 == list.length()) {
    auto *c = new FahrplanTreeItemPlan(plan, list.at(offset), parent);
    parent->appendChild(c);
    return;
  }

  const QString &currentElement = list.at(offset);
  for (auto *item : parent->getChildren()) {
    auto *c = static_cast<FahrplanTreeItemFilePath *>(item);
    if (c->getName() == currentElement) {
      addPath(plan, list, offset + 1, c);
      return;
    }
  }

  auto *c = new FahrplanTreeItemFilePath(currentElement, parent);
  parent->appendChild(c);
  addPath(plan, list, offset + 1, c);
  return;
}

void FahrplanTreeItemPlan::load() {
  QHash<QString, FahrplanTreeItemFahrplangruppe *> map;

  if (loaded) return;

  auto *p = plan.data();
  auto zuege = p->getZuege(*globalZugCache);
  for (const auto &zug : *zuege) {
    FahrplanTreeItemFahrplangruppe *parent;

    if (map.contains(zug->getFahrplanGruppe())) {
      parent = map[zug->getFahrplanGruppe()];
    } else {
      parent =
          new FahrplanTreeItemFahrplangruppe(zug->getFahrplanGruppe(), this);
      appendChild(parent);
      map[zug->getFahrplanGruppe()] = parent;
    }

    auto *c = new FahrplanTreeItemZug(zug, parent);
    parent->appendChild(c);
  }

  loaded = true;
}

const QString &FahrplanTreeItemFahrplangruppe::getName() const { return name; }

QVariant FahrplanTreeItemFahrplangruppe::getData(int column, int role) const {
  if (role == Qt::DisplayRole && column == 0) {
    return name;
  } else {
    return QVariant{};
  }
}

int FahrplanTreeItemZug::columnCount() const { return 2; }

QVariant FahrplanTreeItemZug::getData(int column, int role) const {
  static IconProvider icons{};

  if (role == Qt::DisplayRole && column == 0) {
    return zug->getGattung() + " " + zug->getNummer();
  } else if (role == Qt::DisplayRole && column == 1) {
    return zug->getZuglauf();
  } else if (role == Qt::ForegroundRole && zug->isDeko()) {
    return QColor{Qt::gray};
  } else if (role == Qt::ToolTipRole) {
    return zug->getFilename();
  } else if (role == Qt::DecorationRole && column == 0) {
    return icons(zug->getGattung());
  } else {
    return QVariant{};
  }
}

const Zug &FahrplanTreeItemZug::getZug() const { return *zug; }

void Search::addPredicate(Predicate &&p) { predicates.emplace_back(p); }

static bool applyPedicates(const FahrplanTreeItemZug *current,
                           const std::vector<Search::Predicate> &predicates) {
  for (const auto &predicate : predicates) {
    if (!predicate(current)) return false;
  }

  return true;
}
std::vector<FahrplanTreeItemZug *> Search::search() const {
  std::vector<FahrplanTreeItemZug *> result;
  traverseTrains(root, [this, &result](FahrplanTreeItemZug *tr) {
    if (applyPedicates(tr, predicates)) result.push_back(tr);
  });
  return result;
}
