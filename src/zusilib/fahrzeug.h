#ifndef FAHRZEUG_H
#define FAHRZEUG_H

#include <QDate>
#include <QHash>
#include <QPair>
#include <QString>

class QDataStream;

class Fahrzeug {
 public:
  using VariantenKeyT = QPair<QString, QString>;
  struct Variante {
    QString BR, Beschreibung, Farbgebung, DateiAussenansicht, DateiFuehrerstand;
    QDate EinsatzAb;
  };

  Fahrzeug() {}

  void addVariante(const VariantenKeyT &key, const Variante &variante) {
    variants.insert(key, variante);
  }

  void addVariante(const QString &IDHaupt, const QString &IDNeben,
                   const Variante &variante) {
    addVariante({IDHaupt, IDNeben}, variante);
  }

  auto &getVarianten() const { return variants; }

  QVector<VariantenKeyT> getVariantenNames() const;

  bool varianteExists(const VariantenKeyT &key) {
    return variants.contains(key);
  }

  bool varianteExists(const QString &IDHaupt, const QString &IDNeben) {
    VariantenKeyT key{IDHaupt, IDNeben};
    return varianteExists(key);
  }

  const Variante variante(const VariantenKeyT &key) {
    return variants.value(key);
  }

  const Variante variante(const QString &IDHaupt, const QString &IDNeben) {
    VariantenKeyT key{IDHaupt, IDNeben};
    return variante(key);
  }

  QString filename;

 private:
  QHash<VariantenKeyT, Variante> variants;
};

QDataStream &operator<<(QDataStream &os, const Fahrzeug &fz);
QDataStream &operator>>(QDataStream &is, Fahrzeug &fz);

QDataStream &operator<<(QDataStream &os, const Fahrzeug::Variante &var);
QDataStream &operator>>(QDataStream &is, Fahrzeug::Variante &var);

#endif  // FAHRZEUG_H
