#ifndef FAHRPLANTREE_H
#define FAHRPLANTREE_H

#include <QIcon>
#include <QVariant>
#include <QVector>
#include <functional>
#include "zug.h"
#include "zusifahrplan.h"

class FahrplanTreeItem {
 public:
  enum Type { ROOT, FILE_PATH, PLAN, GRUPPE, TRAIN };

 protected:
  explicit FahrplanTreeItem(Type type, FahrplanTreeItem *parentItem = nullptr);

 public:
  void appendChild(FahrplanTreeItem *child);

 public:
  virtual ~FahrplanTreeItem() { qDeleteAll(childItems); }

  FahrplanTreeItem *getChild(int row);
  FahrplanTreeItem *getChild(int row) const;
  const QVector<FahrplanTreeItem *> getChildren() const { return childItems; }
  int childCount() const;
  virtual int columnCount() const;
  virtual QVariant getData(int column, int role) const = 0;
  int getRow() const;
  FahrplanTreeItem *getParentItem();
  const FahrplanTreeItem *getParentItem() const;
  Type getType() const { return type; }

 private:
  QVector<FahrplanTreeItem *> childItems;
  FahrplanTreeItem *parentItem;
  const Type type;
};

class FahrplanTreeItemRoot : public FahrplanTreeItem {
 public:
  explicit FahrplanTreeItemRoot();
  virtual ~FahrplanTreeItemRoot() {}
  QVariant getData(int, int role) const override {
    if (role == Qt::DisplayRole) {
      return QString("INVALID ") + QString::number(__LINE__);
    } else {
      return QVariant{};
    }
  }
  void addFahrplan(QSharedPointer<ZusiFahrplan> plan);

 private:
  void addPath(QSharedPointer<ZusiFahrplan> plan, const QStringList &list,
               int offset, FahrplanTreeItem *parent);
};

class FahrplanTreeItemFilePath : public FahrplanTreeItem {
 public:
  explicit FahrplanTreeItemFilePath(QString name, FahrplanTreeItem *parentItem)
      : FahrplanTreeItem(FILE_PATH, parentItem), name(name) {}

  const QString &getName() const { return name; }

  QVariant getData(int column, int role) const override {
    if (role == Qt::DisplayRole) {
      if (column == 0) {
        QString display_name = name;
        display_name.replace("_", " ");
        return display_name;
      } else {
        return "";
      }
    }
    return QVariant{};
  }

 private:
  QString name;
};

class FahrplanTreeItemPlan : public FahrplanTreeItem {
 public:
  explicit FahrplanTreeItemPlan(QSharedPointer<ZusiFahrplan> plan,
                                const QString &file,
                                FahrplanTreeItem *parentItem)
      : FahrplanTreeItem(PLAN, parentItem),
        plan(plan),
        file(file),
        loaded(false),
        icon(":/icons/ebula.png") {
    this->file.replace(".fpn", "").replace("_", " ");
  }

  void load();

  QVariant getData(int column, int role) const override {
    if (role == Qt::DisplayRole) {
      if (column == 0) {
        return file;
      } else {
        return "";
      }
    } else if (role == Qt::DecorationRole && column == 0) {
      return icon;
    }

    return QVariant{};
  }

  QSharedPointer<ZusiFahrplan> getPlan() const { return plan; }

 private:
  QSharedPointer<ZusiFahrplan> plan;
  QString file;
  bool loaded;
  QIcon icon;
};

class FahrplanTreeItemFahrplangruppe : public FahrplanTreeItem {
 public:
  explicit FahrplanTreeItemFahrplangruppe(QString name,
                                          FahrplanTreeItem *parentItem)
      : FahrplanTreeItem(GRUPPE, parentItem), name(name) {}

  const QString &getName() const;
  QVariant getData(int column, int role) const override;

 private:
  QString name;
};

class FahrplanTreeItemZug : public FahrplanTreeItem {
 public:
  explicit FahrplanTreeItemZug(const Zug *zug, FahrplanTreeItem *parentItem)
      : FahrplanTreeItem(TRAIN, parentItem), zug{zug} {}

  int columnCount() const override;
  QVariant getData(int column, int role) const override;
  const Zug &getZug() const;

 private:
  const Zug *zug;
};

template <typename CallbackT>
void traverseTree(const FahrplanTreeItem *item, CallbackT cb) {
  auto childCount = item->childCount();
  for (auto i = 0; i < childCount; ++i) {
    auto child = item->getChild(i);
    cb(child);
    traverseTree(child, cb);
  }
}

template <typename CallbackT>
void traverseTrains(const FahrplanTreeItem *item, CallbackT cb) {
  traverseTree(item, [cb](FahrplanTreeItem *item) {
    if (item->getType() == FahrplanTreeItem::TRAIN) {
      auto tr = dynamic_cast<FahrplanTreeItemZug *>(item);
      cb(tr);
    }
  });
}

class Search {
 public:
  using Predicate = std::function<bool(const FahrplanTreeItemZug *)>;

  explicit Search(const FahrplanTreeItem *root) : root(root), predicates() {}
  void addPredicate(Predicate &&p);
  std::vector<FahrplanTreeItemZug *> search() const;

 private:
  const FahrplanTreeItem *root;
  std::vector<Predicate> predicates;
};

#endif  // FAHRPLANTREE_H
