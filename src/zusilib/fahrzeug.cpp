#include "fahrzeug.h"

#include <QDataStream>

QDataStream &operator<<(QDataStream &os, const Fahrzeug &fz) {
  os << fz.filename << fz.getVarianten();
  return os;
}

QDataStream &operator>>(QDataStream &is, Fahrzeug &fz) {
  is >> fz.filename;
  QHash<Fahrzeug::VariantenKeyT, Fahrzeug::Variante> variants;
  is >> variants;

  return is;
}

QDataStream &operator<<(QDataStream &os, const Fahrzeug::Variante &var) {
  os << var.BR << var.Beschreibung << var.Farbgebung << var.DateiAussenansicht
     << var.DateiFuehrerstand << var.EinsatzAb;

  return os;
}

QDataStream &operator>>(QDataStream &is, Fahrzeug::Variante &var) {
  is >> var.BR >> var.Beschreibung >> var.Farbgebung >>
      var.DateiAussenansicht >> var.DateiFuehrerstand >> var.EinsatzAb;

  return is;
}
