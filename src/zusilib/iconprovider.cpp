#include "iconprovider.h"

#include <QFile>
#include <QRegExp>

IconProvider::IconProvider(const QString &root) {
  QFile mapfile(root + "logo.map");
  if (!mapfile.open(QFile::ReadOnly)) {
    return;
  }

  while (!mapfile.atEnd()) {
    QByteArray line = mapfile.readLine();
    auto splits = line.split(':');
    if (splits.length() != 2) {
      continue;
    }

    auto gattung = QString::fromUtf8(splits[0]);
    QIcon icon{root + QString::fromUtf8(splits[1]).trimmed()};

    mapping.insert(gattung, icon);
  }
}

const QIcon &IconProvider::operator()(const QString &gattung_) const {
  static QRegExp regex{"S[0-9]+"};

  QString gattung = gattung_;

  if (gattung.startsWith("ICE")) {
    gattung = "ICE";
  } else if (regex.exactMatch(gattung)) {
    gattung = "S";
  }

  auto it = mapping.find(gattung);

  if (it == mapping.end()) {
    return empty;
  }

  return it.value();
}
