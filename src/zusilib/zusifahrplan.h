#ifndef ZUSIFAHRPLAN_H
#define ZUSIFAHRPLAN_H

#include <QDateTime>
#include <QSharedPointer>
#include <QStringList>
#include <QVector>
#include <memory>
#include <ostream>
#include <string>

#include "zugcache.h"

class ZusiFahrplan {
 public:
  ZusiFahrplan() = default;
  ZusiFahrplan(const ZusiFahrplan &) = delete;
  ZusiFahrplan(ZusiFahrplan &&) = default;
  ~ZusiFahrplan() = default;

  QString filename;
  QDateTime anfangsZeit;
  QString begruessungsDatei;
  std::vector<QString> zugFiles;
  std::vector<QString> strModulFiles;

  const std::vector<const Zug *> *getZuege(ZugCache &zugCache);

 private:
  std::unique_ptr<std::vector<const Zug *>> zuege{nullptr};
};

#endif  // ZUSIFAHRPLAN_H
