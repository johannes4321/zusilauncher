#include "fahrplanfinder.h"

static const QStringList list = QStringList{"*.fpn"};

FahrplanFinder::FahrplanFinder() {}

void FahrplanFinder::find(const QDir &root) {
  for (const QString &match :
       root.entryList(list, QDir::Files | QDir::NoSymLinks)) {
    files.append(root.absolutePath() + QLatin1Char('/') + match);
  }

  for (const QString &dir :
       root.entryList(QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot)) {
    find(root.absolutePath() + QLatin1Char('/') + dir);
  }
}

const QStringList &FahrplanFinder::getList() const { return files; }
