#include "zusifahrplan.h"

const std::vector<const Zug *> *ZusiFahrplan::getZuege(ZugCache &zugCache) {
  if (!zuege) {
    zuege.reset(new std::vector<const Zug *>());
    zuege.get()->reserve(zugFiles.size());
    for (const QString &file : zugFiles) {
      zuege.get()->push_back(&zugCache.getEntry(file));
    }
  }
  return zuege.get();
}
