#ifndef ZUSICACHE_H
#define ZUSICACHE_H

#include "fahrzeugcache.h"
#include "zugcache.h"

class QFile;

class ZusiCache {
 public:
  explicit ZusiCache(const QString &rootPath, QFile &cacheFile);

  ~ZusiCache();

  ZugCache *zugCache() { return &zugCache_; }
  FahrzeugCache *fahrzeugCache() { return &fahrzeugCache_; }

  void persist();

 private:
  static const QString header;
  const QString rootPath;
  QFile &cacheFile;
  ZugCache zugCache_{rootPath};
  FahrzeugCache fahrzeugCache_{rootPath};
};

#endif  // ZUSICACHE_H
